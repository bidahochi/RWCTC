//FMT-Marker FVTM-1.5
package train.client.render.models;

import net.minecraft.entity.Entity;
import tmt.FVTMFormatBase;
import tmt.ModelRendererTurbo;

/** This file was exported via the FVTM Exporter v1.5 of<br>
 *  FMT (Fex's Modelling Toolbox) v.2.7.1 &copy; 2022 - Fexcraft.net<br>
 *  All rights reserved. For this Model's License contact the Author/Creator.
 */
public class ModelDuLokLocomotive extends FVTMFormatBase {

	public ModelDuLokLocomotive(){
		super();
		textureX = 512;
		textureY = 128;
		//
		TurboList group0 = new TurboList("group0");
		group0.add(new ModelRendererTurbo(group0, 1, 1, textureX, textureY).addBox(0, 0, 0, 83, 8, 12)
			.setRotationPoint(-41.5f, 0, -6).setRotationAngle(0, 0, -0).setName("Du loco part01")
		);
		group0.add(new ModelRendererTurbo(group0, 177, 1, textureX, textureY).addBox(0, 0, 0, 1, 3, 22)
			.setRotationPoint(-44.5f, 0, -11).setRotationAngle(0, 0, -0).setName("Du loco part02")
		);
		group0.add(new ModelRendererTurbo(group0, 225, 1, textureX, textureY).addBox(0, 0, 0, 89, 1, 22)
			.setRotationPoint(-44.5f, -1, -11).setRotationAngle(0, 0, -0).setName("Du loco part03")
		);
		group0.add(new ModelRendererTurbo(group0, 449, 1, textureX, textureY).addBox(0, 0, 0, 1, 3, 22)
			.setRotationPoint(43.5f, 0, -11).setRotationAngle(0, 0, -0).setName("Du loco part04")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 25, textureX, textureY).addBox(0, 0, 0, 68, 1, 1)
			.setRotationPoint(-40.5f, 0, 7).setRotationAngle(0, 0, -0).setName("Du loco part05")
		);
		group0.add(new ModelRendererTurbo(group0, 225, 25, textureX, textureY).addBox(0, 0, 0, 68, 1, 1)
			.setRotationPoint(-40.5f, 0, -8).setRotationAngle(0, 0, -0).setName("Du loco part06")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 33, textureX, textureY).addBox(0, 0, 0, 47, 18, 1)
			.setRotationPoint(-23.5f, -19, -11).setRotationAngle(0, 0, -0).setName("Du loco part07")
		);
		group0.add(new ModelRendererTurbo(group0, 105, 33, textureX, textureY).addBox(0, 0, 0, 87, 3, 4)
			.setRotationPoint(-43.5f, -22, -2).setRotationAngle(0, 0, -0).setName("Du loco part08")
		);
		group0.add(new ModelRendererTurbo(group0, 185, 1, textureX, textureY).addBox(0, 0, 0, 6, 6, 0)
			.setRotationPoint(-37.5f, 4, 6.05f).setRotationAngle(0, 0, -0).setName("Du loco part09")
		);
		group0.add(new ModelRendererTurbo(group0, 209, 1, textureX, textureY).addBox(0, 0, 0, 6, 6, 0)
			.setRotationPoint(31.5f, 4, 6.05f).setRotationAngle(0, 0, -0).setName("Du loco part10")
		);
		group0.add(new ModelRendererTurbo(group0, 225, 1, textureX, textureY).addBox(0, 0, 0, 6, 6, 0)
			.setRotationPoint(-37.5f, 4, -6.05f).setRotationAngle(0, 0, -0).setName("Du loco part11")
		);
		group0.add(new ModelRendererTurbo(group0, 433, 1, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(-25.5f, 0, -6.05f).setRotationAngle(0, 0, -0).setName("Du loco part12")
		);
		group0.add(new ModelRendererTurbo(group0, 457, 1, textureX, textureY).addBox(0, 0, 0, 6, 6, 0)
			.setRotationPoint(31.5f, 4, -6.05f).setRotationAngle(0, 0, -0).setName("Du loco part13")
		);
		group0.add(new ModelRendererTurbo(group0, 481, 1, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(-11.5f, 0, -6.05f).setRotationAngle(0, 0, -0).setName("Du loco part14")
		);
		group0.add(new ModelRendererTurbo(group0, 209, 9, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(1.5f, 0, -6.05f).setRotationAngle(0, 0, -0).setName("Du loco part15")
		);
		group0.add(new ModelRendererTurbo(group0, 369, 25, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(16.5f, 0, -6.05f).setRotationAngle(0, 0, -0).setName("Du loco part16")
		);
		group0.add(new ModelRendererTurbo(group0, 393, 33, textureX, textureY).addBox(0, 0, 0, 43, 1, 0)
			.setRotationPoint(-21.5f, 2, -6.1f).setRotationAngle(0, 0, -0).setName("Du loco part17")
		);
		group0.add(new ModelRendererTurbo(group0, 345, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 18, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-43.5f, -19, -10).setRotationAngle(0, 0, -0).setName("Du loco part19")
		);
		group0.add(new ModelRendererTurbo(group0, 81, 33, textureX, textureY).addBox(0, 0, 0, 1, 18, 20)
			.setRotationPoint(42.5f, -19, -10).setRotationAngle(0, 0, -0).setName("Du loco part20")
		);
		group0.add(new ModelRendererTurbo(group0, 289, 33, textureX, textureY).addBox(0, 0, 0, 19, 18, 1)
			.setRotationPoint(-42.5f, -19, 10).setRotationAngle(0, 0, -0).setName("Du loco part22")
		);
		group0.add(new ModelRendererTurbo(group0, 337, 33, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(-25.5f, 0, 6.05f).setRotationAngle(0, 0, -0).setName("Du loco part23")
		);
		group0.add(new ModelRendererTurbo(group0, 481, 33, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(-11.5f, 0, 6.05f).setRotationAngle(0, 0, -0).setName("Du loco part24")
		);
		group0.add(new ModelRendererTurbo(group0, 105, 41, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(1.5f, 0, 6.05f).setRotationAngle(0, 0, -0).setName("Du loco part25")
		);
		group0.add(new ModelRendererTurbo(group0, 129, 41, textureX, textureY).addBox(0, 0, 0, 10, 10, 0)
			.setRotationPoint(16.5f, 0, 6.05f).setRotationAngle(0, 0, -0).setName("Du loco part26")
		);
		group0.add(new ModelRendererTurbo(group0, 153, 41, textureX, textureY).addBox(0, 0, 0, 43, 1, 0)
			.setRotationPoint(-21.5f, 2, 6.1f).setRotationAngle(0, 0, -0).setName("Du loco part27")
		);
		group0.add(new ModelRendererTurbo(group0, 241, 41, textureX, textureY).addBox(0, 0, 0, 19, 18, 1)
			.setRotationPoint(-42.5f, -19, -11).setRotationAngle(0, 0, -0).setName("Du loco part28")
		);
		group0.add(new ModelRendererTurbo(group0, 393, 41, textureX, textureY).addBox(0, 0, 0, 19, 18, 1)
			.setRotationPoint(23.5f, -19, -11).setRotationAngle(0, 0, -0).setName("Du loco part31")
		);
		group0.add(new ModelRendererTurbo(group0, 153, 49, textureX, textureY).addBox(0, 0, 0, 19, 18, 1)
			.setRotationPoint(23.5f, -19, 10).setRotationAngle(0, 0, -0).setName("Du loco part32")
		);
		group0.add(new ModelRendererTurbo(group0, 433, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 7, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1)
			.setRotationPoint(-43.5f, -23, -1).setRotationAngle(0, 0, -0).setName("Du loco part37")
		);
		group0.add(new ModelRendererTurbo(group0, 481, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 7, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1)
			.setRotationPoint(36.5f, -23, -1).setRotationAngle(0, 0, -0).setName("Du loco part38")
		);
		group0.add(new ModelRendererTurbo(group0, 441, 41, textureX, textureY).addBox(0, 0, 0, 1, 2, 16)
			.setRotationPoint(-44.5f, 3, -8).setRotationAngle(0, 0, -0).setName("Du loco part41")
		);
		group0.add(new ModelRendererTurbo(group0, 201, 49, textureX, textureY).addBox(0, 0, 0, 1, 2, 16)
			.setRotationPoint(43.5f, 3, -8).setRotationAngle(0, 0, -0).setName("Du loco part42")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 1, textureX, textureY).addBox(0, 0, 0, 1, 2, 2)
			.setRotationPoint(-46.5f, 3, 5).setRotationAngle(0, 0, -0).setName("Du loco part47")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 1, textureX, textureY).addBox(0, 0, 0, 1, 2, 2)
			.setRotationPoint(-46.5f, 3, -7).setRotationAngle(0, 0, -0).setName("Du loco part56")
		);
		group0.add(new ModelRendererTurbo(group0, 233, 9, textureX, textureY).addBox(0, 0, 0, 1, 2, 2)
			.setRotationPoint(45.5f, 3, 5).setRotationAngle(0, 0, -0).setName("Du loco part57")
		);
		group0.add(new ModelRendererTurbo(group0, 457, 9, textureX, textureY).addBox(0, 0, 0, 1, 2, 2)
			.setRotationPoint(45.5f, 3, -7).setRotationAngle(0, 0, -0).setName("Du loco part62")
		);
		group0.add(new ModelRendererTurbo(group0, 233, 17, textureX, textureY).addBox(0, 0, 0, 0, 0, 4)
			.setRotationPoint(-43.5f, -22, -2).setRotationAngle(0, 0, -0).setName("Du loco part68")
		);
		group0.add(new ModelRendererTurbo(group0, 233, 65, textureX, textureY).addBox(0, 0, 0, 87, 2, 4)
			.setRotationPoint(-43.5f, -21, 2).setRotationAngle(0, 0, -0).setName("Du loco part69")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 73, textureX, textureY).addBox(0, 0, 0, 87, 2, 4)
			.setRotationPoint(-43.5f, -21, -6).setRotationAngle(0, 0, -0).setName("Du loco part71")
		);
		group0.add(new ModelRendererTurbo(group0, 449, 9, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1)
			.setRotationPoint(-43.5f, -21, 6).setRotationAngle(0, 0, -0).setName("Du loco part78")
		);
		group0.add(new ModelRendererTurbo(group0, 497, 9, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 5, 0, 0, -2, -1, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-43.5f, -21, -11).setRotationAngle(0, 0, -0).setName("Du loco part79")
		);
		group0.add(new ModelRendererTurbo(group0, 497, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0, -2, 0, -1, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1)
			.setRotationPoint(43.5f, -21, -6).setRotationAngle(0, 180, -0).setName("Du loco part80")
		);
		group0.add(new ModelRendererTurbo(group0, 137, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 5, 0, 0, -2, -1, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(43.5f, -21, 11).setRotationAngle(0, 180, -0).setName("Du loco part81")
		);
		group0.add(new ModelRendererTurbo(group0, 457, 49, textureX, textureY)
			.addShapeBox(0, 0, 0, 5, 1, 20, 0, 0, 0, 0, -3, 0, 0, -4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-42.5f, -8, -10).setRotationAngle(0, 0, -0).setName("Du loco part82")
		);
		group0.add(new ModelRendererTurbo(group0, 185, 9, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-35.5f, -2, -1).setRotationAngle(0, 0, -0).setName("Du loco part83")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 9, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-34.5f, -2, -1).setRotationAngle(0, -90, -0).setName("Du loco part84")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 9, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-34.5f, -2, 2).setRotationAngle(0, -90, -0).setName("Du loco part85")
		);
		group0.add(new ModelRendererTurbo(group0, 233, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 3, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-35.5f, -5, -1).setRotationAngle(0, 0, -0).setName("Du loco part94")
		);
		group0.add(new ModelRendererTurbo(group0, 457, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-34.5f, -5, 2).setRotationAngle(0, -90, -0).setName("Du loco part95")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-34.5f, -5, -1).setRotationAngle(0, -90, -0).setName("Du loco part97")
		);
		group0.add(new ModelRendererTurbo(group0, 153, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-32.5f, -9, -1).setRotationAngle(0, 0, -0).setName("Du loco part100")
		);
		group0.add(new ModelRendererTurbo(group0, 241, 1, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-32.5f, -8, -2).setRotationAngle(0, 0, -0).setName("Du loco part101")
		);
		group0.add(new ModelRendererTurbo(group0, 241, 9, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-32.5f, -8, 1).setRotationAngle(0, 0, -0).setName("Du loco part102")
		);
		group0.add(new ModelRendererTurbo(group0, 401, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 5, 1, 20, 0, 0, 0, 0, -3, 0, 0, -4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(42.5f, -8, 10).setRotationAngle(0, 180, -0).setName("Du loco part106")
		);
		group0.add(new ModelRendererTurbo(group0, 161, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 3, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(32.5f, -5, -1).setRotationAngle(0, 0, -0).setName("Du loco part107")
		);
		group0.add(new ModelRendererTurbo(group0, 145, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(32.5f, -2, 2).setRotationAngle(0, -90, -0).setName("Du loco part109")
		);
		group0.add(new ModelRendererTurbo(group0, 393, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -2, -1).setRotationAngle(0, 0, -0).setName("Du loco part111")
		);
		group0.add(new ModelRendererTurbo(group0, 409, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(32.5f, -2, -1).setRotationAngle(0, -90, -0).setName("Du loco part112")
		);
		group0.add(new ModelRendererTurbo(group0, 417, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(32.5f, -5, 2).setRotationAngle(0, -90, -0).setName("Du loco part119")
		);
		group0.add(new ModelRendererTurbo(group0, 425, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(32.5f, -5, -1).setRotationAngle(0, -90, -0).setName("Du loco part120")
		);
		group0.add(new ModelRendererTurbo(group0, 433, 25, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -9, -1).setRotationAngle(0, 0, -0).setName("Du loco part121")
		);
		group0.add(new ModelRendererTurbo(group0, 465, 9, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -8, 1).setRotationAngle(0, 0, -0).setName("Du loco part122")
		);
		group0.add(new ModelRendererTurbo(group0, 193, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -8, -2).setRotationAngle(0, 0, -0).setName("Du loco part123")
		);
		group0.add(new ModelRendererTurbo(group0, 169, 25, textureX, textureY).addBox(0, 0, 0, 1, 2, 4)
			.setRotationPoint(-44.5f, -6, 4).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart14")
		);
		group0.add(new ModelRendererTurbo(group0, 441, 25, textureX, textureY).addBox(0, 0, 0, 1, 2, 4)
			.setRotationPoint(-44.5f, -6, -8).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart17")
		);
		group0.add(new ModelRendererTurbo(group0, 497, 25, textureX, textureY).addBox(0, 0, 1, 1, 1, 2)
			.setRotationPoint(-44.5f, -7, -8).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart16")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 25, textureX, textureY).addBox(0, 0, 1, 1, 1, 2)
			.setRotationPoint(-44.5f, -4, -8).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart18")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 33, textureX, textureY).addBox(0, 0, 1, 1, 1, 2)
			.setRotationPoint(-44.5f, -4, 4).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart15")
		);
		group0.add(new ModelRendererTurbo(group0, 369, 41, textureX, textureY).addBox(0, 0, 1, 1, 1, 2)
			.setRotationPoint(-44.5f, -7, 4).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart13")
		);
		group0.add(new ModelRendererTurbo(group0, 377, 41, textureX, textureY).addBox(0, 0, 1, 1, 1, 2)
			.setRotationPoint(-44.5f, -23, -2).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart10")
		);
		group0.add(new ModelRendererTurbo(group0, 385, 41, textureX, textureY).addBox(0, 0, 1, 1, 1, 2)
			.setRotationPoint(-44.5f, -20, -2).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart12")
		);
		group0.add(new ModelRendererTurbo(group0, 441, 41, textureX, textureY).addBox(0, 0, 1, 1, 2, 4)
			.setRotationPoint(-44.5f, -22, -3).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart11")
		);
		group0.add(new ModelRendererTurbo(group0, 465, 41, textureX, textureY).addBox(0, 0, 0, 1, 2, 4)
			.setRotationPoint(43.5f, -6, 4).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart6")
		);
		group0.add(new ModelRendererTurbo(group0, 201, 49, textureX, textureY).addBox(0, 0, 0, 1, 2, 4)
			.setRotationPoint(43.5f, -6, -8).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart4")
		);
		group0.add(new ModelRendererTurbo(group0, 225, 49, textureX, textureY).addBox(0, 0, 0, 1, 2, 4)
			.setRotationPoint(43.5f, -22, -2).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart8")
		);
		group0.add(new ModelRendererTurbo(group0, 449, 41, textureX, textureY).addBox(0, 0, 0, 1, 1, 2)
			.setRotationPoint(43.5f, -23, -1).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart9")
		);
		group0.add(new ModelRendererTurbo(group0, 473, 41, textureX, textureY).addBox(0, 0, 0, 1, 1, 2)
			.setRotationPoint(43.5f, -20, -1).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart7")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 41, textureX, textureY).addBox(0, 0, 0, 1, 1, 2)
			.setRotationPoint(43.5f, -7, -7).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart5")
		);
		group0.add(new ModelRendererTurbo(group0, 209, 49, textureX, textureY).addBox(0, 0, 0, 1, 1, 2)
			.setRotationPoint(43.5f, -4, -7).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart3")
		);
		group0.add(new ModelRendererTurbo(group0, 233, 49, textureX, textureY).addBox(0, 0, 0, 1, 1, 2)
			.setRotationPoint(43.5f, -4, 5).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart2")
		);
		group0.add(new ModelRendererTurbo(group0, 337, 49, textureX, textureY).addBox(0, 0, 0, 1, 1, 2)
			.setRotationPoint(43.5f, -7, 5).setRotationAngle(0, 0, -0).setName("Du loco part Ligjhtpart1")
		);
		group0.add(new ModelRendererTurbo(group0, 185, 73, textureX, textureY)
			.addShapeBox(0, 0, 0, 85, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, -1, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 4)
			.setRotationPoint(-42.5f, -20, 6).setRotationAngle(0, 0, -0).setName("Box 146")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 81, textureX, textureY)
			.addShapeBox(0, 0, 0, 85, 1, 1, 0, 0, 1, -1, 0, 1, -1, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 4, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-42.5f, -20, -7).setRotationAngle(0, 0, -0).setName("Box 147")
		);
		group0.add(new ModelRendererTurbo(group0, 177, 81, textureX, textureY)
			.addShapeBox(0, 0, 0, 87, 1, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 3, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-43.5f, -22, -3).setRotationAngle(0, 0, -0).setName("Box 148")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 87, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 3)
			.setRotationPoint(-43.5f, -22, 2).setRotationAngle(0, 0, -0).setName("Box 149")
		);
		group0.add(new ModelRendererTurbo(group0, 465, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-45.5f, 3, -7).setRotationAngle(0, 0, -0).setName("Box 151")
		);
		group0.add(new ModelRendererTurbo(group0, 441, 49, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-45.5f, 4, -7).setRotationAngle(0, 0, -0).setName("Box 152")
		);
		group0.add(new ModelRendererTurbo(group0, 449, 49, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1)
			.setRotationPoint(-45.5f, 4, -6).setRotationAngle(0, 0, -0).setName("Box 153")
		);
		group0.add(new ModelRendererTurbo(group0, 465, 49, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-45.5f, 3, -6).setRotationAngle(0, 0, -0).setName("Box 154")
		);
		group0.add(new ModelRendererTurbo(group0, 489, 49, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-45.5f, 3, 6).setRotationAngle(0, 0, -0).setName("Box 155")
		);
		group0.add(new ModelRendererTurbo(group0, 497, 49, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-45.5f, 3, 5).setRotationAngle(0, 0, -0).setName("Box 156")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 49, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-45.5f, 4, 5).setRotationAngle(0, 0, -0).setName("Box 157")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1)
			.setRotationPoint(-45.5f, 4, 6).setRotationAngle(0, 0, -0).setName("Box 158")
		);
		group0.add(new ModelRendererTurbo(group0, 9, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1)
			.setRotationPoint(44.5f, 4, 6).setRotationAngle(0, 0, -0).setName("Box 159")
		);
		group0.add(new ModelRendererTurbo(group0, 17, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(44.5f, 4, 5).setRotationAngle(0, 0, -0).setName("Box 160")
		);
		group0.add(new ModelRendererTurbo(group0, 25, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(44.5f, 3, 5).setRotationAngle(0, 0, -0).setName("Box 161")
		);
		group0.add(new ModelRendererTurbo(group0, 33, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(44.5f, 3, 6).setRotationAngle(0, 0, -0).setName("Box 162")
		);
		group0.add(new ModelRendererTurbo(group0, 41, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1)
			.setRotationPoint(44.5f, 4, -6).setRotationAngle(0, 0, -0).setName("Box 163")
		);
		group0.add(new ModelRendererTurbo(group0, 49, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(44.5f, 4, -7).setRotationAngle(0, 0, -0).setName("Box 164")
		);
		group0.add(new ModelRendererTurbo(group0, 57, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(44.5f, 3, -7).setRotationAngle(0, 0, -0).setName("Box 165")
		);
		group0.add(new ModelRendererTurbo(group0, 65, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(44.5f, 3, -6).setRotationAngle(0, 0, -0).setName("Box 166")
		);
		group0.add(new ModelRendererTurbo(group0, 433, 65, textureX, textureY)
			.addShapeBox(0, 0, 1, 1, 18, 1, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-43.5f, -19, -12).setRotationAngle(0, 0, -0).setName("Box 167")
		);
		group0.add(new ModelRendererTurbo(group0, 441, 65, textureX, textureY)
			.addShapeBox(0, 0, 1, 1, 18, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1)
			.setRotationPoint(-43.5f, -19, 9).setRotationAngle(0, 0, -0).setName("Box 168")
		);
		group0.add(new ModelRendererTurbo(group0, 449, 65, textureX, textureY)
			.addShapeBox(0, 0, 1, 1, 18, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0)
			.setRotationPoint(42.5f, -19, 9).setRotationAngle(0, 0, -0).setName("Box 169")
		);
		group0.add(new ModelRendererTurbo(group0, 361, 73, textureX, textureY)
			.addShapeBox(0, 0, 1, 1, 18, 1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(42.5f, -19, -12).setRotationAngle(0, 0, -0).setName("Box 170")
		);
		group0.add(new ModelRendererTurbo(group0, 73, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1)
			.setRotationPoint(43.5f, 3, 8).setRotationAngle(0, 0, -0).setName("Box 171")
		);
		group0.add(new ModelRendererTurbo(group0, 129, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(43.5f, 3, -9).setRotationAngle(0, 0, -0).setName("Box 172")
		);
		group0.add(new ModelRendererTurbo(group0, 137, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-44.5f, 3, -9).setRotationAngle(0, 0, -0).setName("Box 173")
		);
		group0.add(new ModelRendererTurbo(group0, 145, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1)
			.setRotationPoint(-44.5f, 3, 8).setRotationAngle(0, 0, -0).setName("Box 174")
		);
		group0.add(new ModelRendererTurbo(group0, 201, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-35.5f, -5, -1).setRotationAngle(0, -90, -0).setName("Box 175")
		);
		group0.add(new ModelRendererTurbo(group0, 209, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-35.5f, -5, 2).setRotationAngle(0, -90, -0).setName("Box 176")
		);
		group0.add(new ModelRendererTurbo(group0, 449, 17, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0)
			.setRotationPoint(-32.5f, -5, -2).setRotationAngle(0, 0, -0).setName("Box 177")
		);
		group0.add(new ModelRendererTurbo(group0, 225, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-32.5f, -9, 1).setRotationAngle(0, 0, -0).setName("Box 178")
		);
		group0.add(new ModelRendererTurbo(group0, 233, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-32.5f, -9, -2).setRotationAngle(0, 0, -0).setName("Box 179")
		);
		group0.add(new ModelRendererTurbo(group0, 289, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -9, -2).setRotationAngle(0, 0, -0).setName("Box 180")
		);
		group0.add(new ModelRendererTurbo(group0, 297, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -9, 1).setRotationAngle(0, 0, -0).setName("Box 181")
		);
		group0.add(new ModelRendererTurbo(group0, 305, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1)
			.setRotationPoint(34.5f, -5, 2).setRotationAngle(0, -90, -0).setName("Box 182")
		);
		group0.add(new ModelRendererTurbo(group0, 313, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0)
			.setRotationPoint(34.5f, -5, -1).setRotationAngle(0, -90, -0).setName("Box 183")
		);
		group0.add(new ModelRendererTurbo(group0, 321, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -5, 2).setRotationAngle(0, -90, -0).setName("Box 184")
		);
		group0.add(new ModelRendererTurbo(group0, 337, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -2, -1).setRotationAngle(0, -90, -0).setName("Box 185")
		);
		group0.add(new ModelRendererTurbo(group0, 489, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(31.5f, -2, 2).setRotationAngle(0, -90, -0).setName("Box 186")
		);
		group0.add(new ModelRendererTurbo(group0, 497, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0)
			.setRotationPoint(34.5f, -2, 2).setRotationAngle(0, -90, -0).setName("Box 187")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 57, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0)
			.setRotationPoint(34.5f, -2, -1).setRotationAngle(0, -90, -0).setName("Box 188")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0)
			.setRotationPoint(-32.5f, -2, 2).setRotationAngle(0, -90, -0).setName("Box 189")
		);
		group0.add(new ModelRendererTurbo(group0, 9, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-35.5f, -2, 2).setRotationAngle(0, -90, -0).setName("Box 190")
		);
		group0.add(new ModelRendererTurbo(group0, 17, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-35.5f, -2, -1).setRotationAngle(0, -90, -0).setName("Box 191")
		);
		group0.add(new ModelRendererTurbo(group0, 25, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0)
			.setRotationPoint(-32.5f, -2, -1).setRotationAngle(0, -90, -0).setName("Box 192")
		);
		group0.add(new ModelRendererTurbo(group0, 33, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0)
			.setRotationPoint(-33.5f, -4, 1).setRotationAngle(0, -90, -0).setName("Box 193")
		);
		group0.add(new ModelRendererTurbo(group0, 41, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0)
			.setRotationPoint(-33.5f, -4, 0).setRotationAngle(0, -90, -0).setName("Box 194")
		);
		group0.add(new ModelRendererTurbo(group0, 49, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-34.5f, -4, 0).setRotationAngle(0, -90, -0).setName("Box 195")
		);
		group0.add(new ModelRendererTurbo(group0, 57, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-34.5f, -4, 1).setRotationAngle(0, -90, -0).setName("Box 196")
		);
		group0.add(new ModelRendererTurbo(group0, 65, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0)
			.setRotationPoint(33.5f, -4, 1).setRotationAngle(0, -90, -0).setName("Box 197")
		);
		group0.add(new ModelRendererTurbo(group0, 73, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(32.5f, -4, 0).setRotationAngle(0, -90, -0).setName("Box 198")
		);
		group0.add(new ModelRendererTurbo(group0, 129, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0)
			.setRotationPoint(33.5f, -4, 0).setRotationAngle(0, -90, -0).setName("Box 199")
		);
		group0.add(new ModelRendererTurbo(group0, 137, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 1, 2, 1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(32.5f, -4, 1).setRotationAngle(0, -90, -0).setName("Box 200")
		);
		group0.add(new ModelRendererTurbo(group0, 185, 89, textureX, textureY).addBox(0, 0, 0, 47, 18, 1)
			.setRotationPoint(-23.5f, -19, 10).setRotationAngle(0, 0, -0).setName("Box 135")
		);
		group0.add(new ModelRendererTurbo(group0, 353, 73, textureX, textureY).addBox(0, 0, 0, 1, 18, 20)
			.setRotationPoint(-23.5f, -19, -10).setRotationAngle(0, 0, -0).setName("Box 136")
		);
		group0.add(new ModelRendererTurbo(group0, 441, 73, textureX, textureY).addBox(0, 0, 0, 1, 18, 20)
			.setRotationPoint(22.5f, -19, -10).setRotationAngle(0, 0, -0).setName("Box 137")
		);
		group0.add(new ModelRendererTurbo(group0, 377, 73, textureX, textureY).addBox(0, 0, 0, 4, 4, 4)
			.setRotationPoint(-21.5f, -5, -2).setRotationAngle(0, 0, -0).setName("Box 138")
		);
		group0.add(new ModelRendererTurbo(group0, 465, 73, textureX, textureY).addBox(0, 0, 0, 6, 6, 6)
			.setRotationPoint(-22.5f, -11, -3).setRotationAngle(0, 0, -0).setName("Box 139")
		);
		group0.add(new ModelRendererTurbo(group0, 289, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 6, 2, 6, 0, -1, 0, -1, -1, 0, -1, -1, 0, -1, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-22.5f, -13, -3).setRotationAngle(0, 0, -0).setName("Box 141")
		);
		group0.add(new ModelRendererTurbo(group0, 401, 73, textureX, textureY).addBox(0, 0, 0, 4, 1, 4)
			.setRotationPoint(-21.5f, -14, -2).setRotationAngle(0, 0, -0).setName("Box 142")
		);
		group0.add(new ModelRendererTurbo(group0, 321, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 6, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, -1, 0, -1, -1, 0, -1, -1, 0, -1)
			.setRotationPoint(-22.5f, -15, -3).setRotationAngle(0, 0, -0).setName("Box 143")
		);
		group0.add(new ModelRendererTurbo(group0, 401, 89, textureX, textureY).addBox(0, 0, 0, 6, 4, 6)
			.setRotationPoint(-22.5f, -19, -3).setRotationAngle(0, 0, -0).setName("Box 144")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 97, textureX, textureY).addBox(0, 0, 0, 12, 15, 12)
			.setRotationPoint(-13.5f, -16, -5).setRotationAngle(0, 0, -0).setName("Box 145")
		);
		group0.add(new ModelRendererTurbo(group0, 57, 97, textureX, textureY).addBox(0, 0, 0, 14, 1, 14)
			.setRotationPoint(-14.5f, -17, -6).setRotationAngle(0, 0, -0).setName("Box 146")
		);
		group0.add(new ModelRendererTurbo(group0, 457, 65, textureX, textureY).addBox(0, 0, 0, 5, 1, 1)
			.setRotationPoint(-19.5f, -8, -4).setRotationAngle(0, 0, -0).setName("Box 147")
		);
		group0.add(new ModelRendererTurbo(group0, 497, 73, textureX, textureY).addBox(0, 0, 0, 1, 7, 1)
			.setRotationPoint(-15.5f, -15, -4).setRotationAngle(0, 0, -0).setName("Box 148")
		);
		group0.add(new ModelRendererTurbo(group0, 145, 65, textureX, textureY).addBox(0, 0, 0, 1, 1, 1)
			.setRotationPoint(-14.5f, -15, -4).setRotationAngle(0, 0, -0).setName("Box 149")
		);
		group0.add(new ModelRendererTurbo(group0, 489, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 4, 4, 0, -1, 0, -1, -1, 0, -1, -1, 0, -1, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-0.5f, -5, -2).setRotationAngle(0, 0, -0).setName("Box 150")
		);
		group0.add(new ModelRendererTurbo(group0, 41, 97, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 4, 4, 0, -1, 0, -1, -1, 0, -1, -1, 0, -1, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(9.5f, -5, -2).setRotationAngle(0, 0, -0).setName("Box 151")
		);
		group0.add(new ModelRendererTurbo(group0, 121, 97, textureX, textureY).addBox(0, 0, 0, 2, 14, 2)
			.setRotationPoint(10.5f, -19, -1).setRotationAngle(0, 0, -0).setName("Box 152")
		);
		group0.add(new ModelRendererTurbo(group0, 137, 97, textureX, textureY).addBox(0, 0, 0, 2, 14, 2)
			.setRotationPoint(0.5f, -19, -1).setRotationAngle(0, 0, -0).setName("Box 153")
		);
		group0.add(new ModelRendererTurbo(group0, 1, 97, textureX, textureY).addBox(0, 0, 0, 2, 9, 2)
			.setRotationPoint(3.5f, -16, -1).setRotationAngle(0, 0, -0).setName("Box 154")
		);
		group0.add(new ModelRendererTurbo(group0, 105, 97, textureX, textureY).addBox(0, 0, 0, 2, 9, 2)
			.setRotationPoint(7.5f, -16, -1).setRotationAngle(0, 0, -0).setName("Box 155")
		);
		group0.add(new ModelRendererTurbo(group0, 153, 97, textureX, textureY)
			.addShapeBox(0, 0, 0, 2, 6, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1)
			.setRotationPoint(7.5f, -7, -1).setRotationAngle(0, 0, -0).setName("Box 156")
		);
		group0.add(new ModelRendererTurbo(group0, 169, 97, textureX, textureY)
			.addShapeBox(0, 0, 0, 2, 6, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1)
			.setRotationPoint(3.5f, -7, -1).setRotationAngle(0, 0, -0).setName("Box 157")
		);
		group0.add(new ModelRendererTurbo(group0, 313, 89, textureX, textureY).addBox(0, 0, 0, 2, 2, 2)
			.setRotationPoint(5.5f, -15, -1).setRotationAngle(0, 0, -0).setName("Box 158")
		);
		group0.add(new ModelRendererTurbo(group0, 489, 73, textureX, textureY).addBox(0, 0, 0, 1, 2, 2)
			.setRotationPoint(9.5f, -15, -1).setRotationAngle(0, 0, -0).setName("Box 159")
		);
		group0.add(new ModelRendererTurbo(group0, 505, 73, textureX, textureY).addBox(0, 0, 0, 1, 2, 2)
			.setRotationPoint(2.5f, -15, -1).setRotationAngle(0, 0, -0).setName("Box 160")
		);
		group0.add(new ModelRendererTurbo(group0, 489, 65, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -16, 11).setRotationAngle(0, 0, -0).setName("Box 162")
		);
		group0.add(new ModelRendererTurbo(group0, 393, 73, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -14, 11).setRotationAngle(0, 0, -0).setName("Box 163")
		);
		group0.add(new ModelRendererTurbo(group0, 393, 81, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -15, 11).setRotationAngle(0, 0, -0).setName("Box 164")
		);
		group0.add(new ModelRendererTurbo(group0, 409, 81, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -13, 11).setRotationAngle(0, 0, -0).setName("Box 166")
		);
		group0.add(new ModelRendererTurbo(group0, 345, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -13, 11).setRotationAngle(0, 0, -0).setName("Box 167")
		);
		group0.add(new ModelRendererTurbo(group0, 377, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -14, 11).setRotationAngle(0, 0, -0).setName("Box 169")
		);
		group0.add(new ModelRendererTurbo(group0, 393, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -15, 11).setRotationAngle(0, 0, -0).setName("Box 170")
		);
		group0.add(new ModelRendererTurbo(group0, 425, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -16, 11).setRotationAngle(0, 0, -0).setName("Box 171")
		);
		group0.add(new ModelRendererTurbo(group0, 441, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -13, 11).setRotationAngle(0, 0, -0).setName("Box 175")
		);
		group0.add(new ModelRendererTurbo(group0, 465, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -14, 11).setRotationAngle(0, 0, -0).setName("Box 176")
		);
		group0.add(new ModelRendererTurbo(group0, 481, 89, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -13, 11).setRotationAngle(0, 0, -0).setName("Box 177")
		);
		group0.add(new ModelRendererTurbo(group0, 57, 97, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -14, 11).setRotationAngle(0, 0, -0).setName("Box 178")
		);
		group0.add(new ModelRendererTurbo(group0, 313, 97, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -16, 11).setRotationAngle(0, 0, -0).setName("Box 179")
		);
		group0.add(new ModelRendererTurbo(group0, 329, 97, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -15, 11).setRotationAngle(0, 0, -0).setName("Box 180")
		);
		group0.add(new ModelRendererTurbo(group0, 57, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -15, 11).setRotationAngle(0, 0, -0).setName("Box 181")
		);
		group0.add(new ModelRendererTurbo(group0, 289, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -16, 11).setRotationAngle(0, 0, -0).setName("Box 182")
		);
		group0.add(new ModelRendererTurbo(group0, 305, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -16, -12).setRotationAngle(0, 0, -0).setName("Box 185")
		);
		group0.add(new ModelRendererTurbo(group0, 321, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -16, -12).setRotationAngle(0, 0, -0).setName("Box 186")
		);
		group0.add(new ModelRendererTurbo(group0, 337, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -13, -12).setRotationAngle(0, 0, -0).setName("Box 187")
		);
		group0.add(new ModelRendererTurbo(group0, 401, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -14, -12).setRotationAngle(0, 0, -0).setName("Box 188")
		);
		group0.add(new ModelRendererTurbo(group0, 417, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -13, -12).setRotationAngle(0, 0, -0).setName("Box 189")
		);
		group0.add(new ModelRendererTurbo(group0, 489, 105, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -14, -12).setRotationAngle(0, 0, -0).setName("Box 190")
		);
		group0.add(new ModelRendererTurbo(group0, 57, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -16, -12).setRotationAngle(0, 0, -0).setName("Box 191")
		);
		group0.add(new ModelRendererTurbo(group0, 73, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -15, -12).setRotationAngle(0, 0, -0).setName("Box 192")
		);
		group0.add(new ModelRendererTurbo(group0, 89, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(6.5f, -15, -12).setRotationAngle(0, 0, -0).setName("Box 193")
		);
		group0.add(new ModelRendererTurbo(group0, 105, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(11.5f, -16, -12).setRotationAngle(0, 0, -0).setName("Box 194")
		);
		group0.add(new ModelRendererTurbo(group0, 145, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -15, -12).setRotationAngle(0, 0, -0).setName("Box 195")
		);
		group0.add(new ModelRendererTurbo(group0, 161, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -15, -12).setRotationAngle(0, 0, -0).setName("Box 196")
		);
		group0.add(new ModelRendererTurbo(group0, 177, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -14, -12).setRotationAngle(0, 0, -0).setName("Box 197")
		);
		group0.add(new ModelRendererTurbo(group0, 193, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -14, -12).setRotationAngle(0, 0, -0).setName("Box 198")
		);
		group0.add(new ModelRendererTurbo(group0, 209, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-17.5f, -13, -12).setRotationAngle(0, 0, -0).setName("Box 199")
		);
		group0.add(new ModelRendererTurbo(group0, 225, 113, textureX, textureY)
			.addShapeBox(0, 0, 0, 4, 1, 1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
			.setRotationPoint(-12.5f, -13, -12).setRotationAngle(0, 0, -0).setName("Box 200")
		);
		groups.add(group0);
		//
	}

}
