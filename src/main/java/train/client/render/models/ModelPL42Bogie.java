//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 23.12.2021 - 22:47:28
// Last changed on: 23.12.2021 - 22:47:28

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.render.RenderRollingStock;
import train.common.api.AbstractTrains;
import train.common.library.Info;


public class ModelPL42Bogie extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 64;

	public ModelPL42Bogie() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[34];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 192
		bodyModel[1] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // Box 193
		bodyModel[2] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Box 194
		bodyModel[3] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 195
		bodyModel[4] = new ModelRendererTurbo(this, 81, 1, textureX, textureY); // Box 197
		bodyModel[5] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // Box 7
		bodyModel[6] = new ModelRendererTurbo(this, 121, 1, textureX, textureY); // Box 9
		bodyModel[7] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // Box 21
		bodyModel[8] = new ModelRendererTurbo(this, 153, 1, textureX, textureY); // Box 26
		bodyModel[9] = new ModelRendererTurbo(this, 193, 1, textureX, textureY); // Box 12
		bodyModel[10] = new ModelRendererTurbo(this, 201, 1, textureX, textureY); // Box 13
		bodyModel[11] = new ModelRendererTurbo(this, 209, 1, textureX, textureY); // Box 14
		bodyModel[12] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 16
		bodyModel[13] = new ModelRendererTurbo(this, 241, 1, textureX, textureY); // Box 17
		bodyModel[14] = new ModelRendererTurbo(this, 249, 1, textureX, textureY); // Box 18
		bodyModel[15] = new ModelRendererTurbo(this, 257, 1, textureX, textureY); // Box 19
		bodyModel[16] = new ModelRendererTurbo(this, 265, 1, textureX, textureY); // Box 20
		bodyModel[17] = new ModelRendererTurbo(this, 273, 1, textureX, textureY); // Box 21
		bodyModel[18] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 22
		bodyModel[19] = new ModelRendererTurbo(this, 289, 1, textureX, textureY); // Box 23
		bodyModel[20] = new ModelRendererTurbo(this, 329, 1, textureX, textureY); // Box 24
		bodyModel[21] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // Box 25
		bodyModel[22] = new ModelRendererTurbo(this, 345, 1, textureX, textureY); // Box 26
		bodyModel[23] = new ModelRendererTurbo(this, 361, 1, textureX, textureY); // Box 27
		bodyModel[24] = new ModelRendererTurbo(this, 369, 1, textureX, textureY); // Box 28
		bodyModel[25] = new ModelRendererTurbo(this, 377, 1, textureX, textureY); // Box 29
		bodyModel[26] = new ModelRendererTurbo(this, 385, 1, textureX, textureY); // Box 30
		bodyModel[27] = new ModelRendererTurbo(this, 401, 1, textureX, textureY); // Box 31
		bodyModel[28] = new ModelRendererTurbo(this, 409, 1, textureX, textureY); // Box 32
		bodyModel[29] = new ModelRendererTurbo(this, 417, 1, textureX, textureY); // Box 33
		bodyModel[30] = new ModelRendererTurbo(this, 425, 1, textureX, textureY); // Box 34
		bodyModel[31] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 35
		bodyModel[32] = new ModelRendererTurbo(this, 441, 1, textureX, textureY); // Box 36
		bodyModel[33] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Box 39

		bodyModel[0].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 192
		bodyModel[0].setRotationPoint(-11F, 4F, 6F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 193
		bodyModel[1].setRotationPoint(3F, 4F, 6F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 194
		bodyModel[2].setRotationPoint(-11F, 4F, -7F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 8, 6, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 195
		bodyModel[3].setRotationPoint(3F, 4F, -7F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 197
		bodyModel[4].setRotationPoint(6F, 6F, -6F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 7
		bodyModel[5].setRotationPoint(-8F, 6F, -8F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 9
		bodyModel[6].setRotationPoint(6F, 6F, -8F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,-0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F); // Box 21
		bodyModel[7].setRotationPoint(-1F, 2F, -1F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 18, 2, 1, 0F,-5.5F, 0.25F, -0.25F, -5.5F, 0.25F, -0.25F, -5.25F, 0.25F, 0F, -5.5F, 0.25F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 26
		bodyModel[8].setRotationPoint(-9F, 4F, -8F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 12
		bodyModel[9].setRotationPoint(-9F, 5F, -8F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 13
		bodyModel[10].setRotationPoint(7F, 5F, -8F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,-0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 14
		bodyModel[11].setRotationPoint(-2F, 5F, -8F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F,-0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 16
		bodyModel[12].setRotationPoint(-3F, 7F, -8F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 17
		bodyModel[13].setRotationPoint(5F, 5F, -8F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 18
		bodyModel[14].setRotationPoint(-7F, 5F, -8F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F,-0.5F, -0.5F, -0.25F, -1F, -0.5F, -0.25F, -1F, -0.5F, 0F, -0.5F, -0.5F, 0F, -3.5F, -0.5F, -0.25F, 1.5F, -1.5F, -0.25F, 1.5F, -1.5F, 0F, -3.5F, -0.5F, 0F); // Box 19
		bodyModel[15].setRotationPoint(-6F, 5F, -8F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F,-1F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -1F, -0.5F, 0F, 1.5F, -1.5F, -0.25F, -3.5F, -0.5F, -0.25F, -3.5F, -0.5F, 0F, 1.5F, -1.5F, 0F); // Box 20
		bodyModel[16].setRotationPoint(4F, 5F, -8F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-1.25F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -1.25F, -0.5F, 0F, -1.25F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -1.25F, -0.5F, 0F); // Box 21
		bodyModel[17].setRotationPoint(-3F, 5F, -8F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, -0.25F, -1.25F, -0.5F, -0.25F, -1.25F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -1.25F, -0.5F, -0.25F, -1.25F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 22
		bodyModel[18].setRotationPoint(1F, 5F, -8F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 18, 2, 1, 0F,-5.5F, 0.25F, 0F, -5.25F, 0.25F, 0F, -5.5F, 0.25F, -0.25F, -5.5F, 0.25F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 23
		bodyModel[19].setRotationPoint(-9F, 4F, 7F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 24
		bodyModel[20].setRotationPoint(-9F, 5F, 7F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 25
		bodyModel[21].setRotationPoint(-7F, 5F, 7F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 6, 2, 1, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 26
		bodyModel[22].setRotationPoint(-3F, 7F, 7F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F,-0.5F, -0.5F, 0F, -1F, -0.5F, 0F, -1F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -3.5F, -0.5F, 0F, 1.5F, -1.5F, 0F, 1.5F, -1.5F, -0.25F, -3.5F, -0.5F, -0.25F); // Box 27
		bodyModel[23].setRotationPoint(-6F, 5F, 7F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-1.25F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -1.25F, -0.5F, -0.25F, -1.25F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -1.25F, -0.5F, -0.25F); // Box 28
		bodyModel[24].setRotationPoint(-3F, 5F, 7F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, 0F, -1.25F, -0.5F, 0F, -1.25F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -1.25F, -0.5F, 0F, -1.25F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 29
		bodyModel[25].setRotationPoint(1F, 5F, 7F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 4, 3, 1, 0F,-0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F); // Box 30
		bodyModel[26].setRotationPoint(-2F, 5F, 7F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 2, 4, 1, 0F,-1F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -1F, -0.5F, -0.25F, 1.5F, -1.5F, 0F, -3.5F, -0.5F, 0F, -3.5F, -0.5F, -0.25F, 1.5F, -1.5F, -0.25F); // Box 31
		bodyModel[27].setRotationPoint(4F, 5F, 7F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 32
		bodyModel[28].setRotationPoint(5F, 5F, 7F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 2, 3, 1, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -0.25F); // Box 33
		bodyModel[29].setRotationPoint(7F, 5F, 7F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 34
		bodyModel[30].setRotationPoint(6F, 6F, 6F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 2, 2, 2, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 35
		bodyModel[31].setRotationPoint(-8F, 6F, 6F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 2, 1, 14, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		bodyModel[32].setRotationPoint(-1F, 4F, -7F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 2, 2, 12, 0F,-0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F); // Box 39
		bodyModel[33].setRotationPoint(-8F, 6F, -6F);
	}
}