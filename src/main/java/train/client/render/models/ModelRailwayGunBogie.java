//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 02.04.2021 - 15:30:25
// Last changed on: 02.04.2021 - 15:30:25

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.render.RenderRollingStock;
import train.common.api.AbstractTrains;
import train.common.entity.rollingStock.PassengerPL42;
import train.common.library.Info;

public class ModelRailwayGunBogie extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelRailwayGunBogie() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[35];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 5
		bodyModel[1] = new ModelRendererTurbo(this, 41, 1, textureX, textureY); // Box 7
		bodyModel[2] = new ModelRendererTurbo(this, 81, 1, textureX, textureY); // Box 10
		bodyModel[3] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 21
		bodyModel[4] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // Box 22
		bodyModel[5] = new ModelRendererTurbo(this, 33, 1, textureX, textureY); // Box 23
		bodyModel[6] = new ModelRendererTurbo(this, 225, 1, textureX, textureY); // Box 24
		bodyModel[7] = new ModelRendererTurbo(this, 41, 1, textureX, textureY); // Box 25
		bodyModel[8] = new ModelRendererTurbo(this, 65, 1, textureX, textureY); // Box 26
		bodyModel[9] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 27
		bodyModel[10] = new ModelRendererTurbo(this, 81, 1, textureX, textureY); // Box 28
		bodyModel[11] = new ModelRendererTurbo(this, 105, 1, textureX, textureY); // Box 29
		bodyModel[12] = new ModelRendererTurbo(this, 113, 1, textureX, textureY); // Box 30
		bodyModel[13] = new ModelRendererTurbo(this, 281, 1, textureX, textureY); // Box 31
		bodyModel[14] = new ModelRendererTurbo(this, 121, 1, textureX, textureY); // Box 32
		bodyModel[15] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 33
		bodyModel[16] = new ModelRendererTurbo(this, 129, 1, textureX, textureY); // Box 34
		bodyModel[17] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 35
		bodyModel[18] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 36
		bodyModel[19] = new ModelRendererTurbo(this, 169, 1, textureX, textureY); // Box 37
		bodyModel[20] = new ModelRendererTurbo(this, 201, 1, textureX, textureY); // Box 38
		bodyModel[21] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // Box 39
		bodyModel[22] = new ModelRendererTurbo(this, 345, 1, textureX, textureY); // Box 40
		bodyModel[23] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // Box 41
		bodyModel[24] = new ModelRendererTurbo(this, 369, 1, textureX, textureY); // Box 42
		bodyModel[25] = new ModelRendererTurbo(this, 201, 9, textureX, textureY); // Box 75
		bodyModel[26] = new ModelRendererTurbo(this, 225, 9, textureX, textureY); // Box 76
		bodyModel[27] = new ModelRendererTurbo(this, 249, 9, textureX, textureY); // Box 77
		bodyModel[28] = new ModelRendererTurbo(this, 337, 9, textureX, textureY); // Box 78
		bodyModel[29] = new ModelRendererTurbo(this, 497, 9, textureX, textureY); // Box 89
		bodyModel[30] = new ModelRendererTurbo(this, 289, 17, textureX, textureY); // Box 90
		bodyModel[31] = new ModelRendererTurbo(this, 305, 17, textureX, textureY); // Box 91
		bodyModel[32] = new ModelRendererTurbo(this, 321, 17, textureX, textureY); // Box 92
		bodyModel[33] = new ModelRendererTurbo(this, 337, 17, textureX, textureY); // Box 93
		bodyModel[34] = new ModelRendererTurbo(this, 353, 17, textureX, textureY); // Box 94

		bodyModel[0].addShapeBox(0F, 0F, -2F, 1, 1, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 5
		bodyModel[0].setRotationPoint(-9.5F, 7F, -5.5F);

		bodyModel[1].addShapeBox(0F, 0F, -2F, 1, 1, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		bodyModel[1].setRotationPoint(-0.5F, 7F, -5.5F);

		bodyModel[2].addShapeBox(0F, 0F, -2F, 1, 1, 15, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		bodyModel[2].setRotationPoint(8.5F, 7F, -5.5F);

		bodyModel[3].addShapeBox(0F, 0F, -2F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		bodyModel[3].setRotationPoint(-10.5F, 5F, 8.5F);

		bodyModel[4].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 22
		bodyModel[4].setRotationPoint(-11.5F, 5F, 8.5F);

		bodyModel[5].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 23
		bodyModel[5].setRotationPoint(-7.5F, 5F, 8.5F);

		bodyModel[6].addShapeBox(0F, 0F, -2F, 25, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 24
		bodyModel[6].setRotationPoint(-12.5F, 4F, 8.5F);

		bodyModel[7].addShapeBox(0F, 0F, -2F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		bodyModel[7].setRotationPoint(-1.5F, 5F, 8.5F);

		bodyModel[8].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 26
		bodyModel[8].setRotationPoint(1.5F, 5F, 8.5F);

		bodyModel[9].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 27
		bodyModel[9].setRotationPoint(-2.5F, 5F, 8.5F);

		bodyModel[10].addShapeBox(0F, 0F, -2F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 28
		bodyModel[10].setRotationPoint(7.5F, 5F, 8.5F);

		bodyModel[11].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 29
		bodyModel[11].setRotationPoint(10.5F, 5F, 8.5F);

		bodyModel[12].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 30
		bodyModel[12].setRotationPoint(6.5F, 5F, 8.5F);

		bodyModel[13].addShapeBox(0F, 0F, -2F, 25, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 31
		bodyModel[13].setRotationPoint(-12.5F, 4F, -5.5F);

		bodyModel[14].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 32
		bodyModel[14].setRotationPoint(10.5F, 5F, -5.5F);

		bodyModel[15].addShapeBox(0F, 0F, -2F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		bodyModel[15].setRotationPoint(7.5F, 5F, -5.5F);

		bodyModel[16].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 34
		bodyModel[16].setRotationPoint(6.5F, 5F, -5.5F);

		bodyModel[17].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 35
		bodyModel[17].setRotationPoint(1.5F, 5F, -5.5F);

		bodyModel[18].addShapeBox(0F, 0F, -2F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 36
		bodyModel[18].setRotationPoint(-1.5F, 5F, -5.5F);

		bodyModel[19].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 37
		bodyModel[19].setRotationPoint(-2.5F, 5F, -5.5F);

		bodyModel[20].addShapeBox(0F, 0F, -2F, 3, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 38
		bodyModel[20].setRotationPoint(-10.5F, 5F, -5.5F);

		bodyModel[21].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F); // Box 39
		bodyModel[21].setRotationPoint(-11.5F, 5F, -5.5F);

		bodyModel[22].addShapeBox(0F, 0F, -2F, 1, 2, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F); // Box 40
		bodyModel[22].setRotationPoint(-7.5F, 5F, -5.5F);

		bodyModel[23].addShapeBox(0F, 0F, -2F, 2, 1, 13, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 41
		bodyModel[23].setRotationPoint(3.5F, 4F, -4.5F);

		bodyModel[24].addShapeBox(0F, 0F, -2F, 2, 1, 13, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 42
		bodyModel[24].setRotationPoint(-5.5F, 4F, -4.5F);

		bodyModel[25].addShapeBox(0F, 0F, -2F, 4, 1, 1, 0F,0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 75
		bodyModel[25].setRotationPoint(-6.5F, 6F, -5.5F);

		bodyModel[26].addShapeBox(0F, 0F, -2F, 4, 1, 1, 0F,0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 76
		bodyModel[26].setRotationPoint(2.5F, 6F, -5.5F);

		bodyModel[27].addShapeBox(0F, 0F, -2F, 4, 1, 1, 0F,0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 77
		bodyModel[27].setRotationPoint(-6.5F, 6F, 8.5F);

		bodyModel[28].addShapeBox(0F, 0F, -2F, 4, 1, 1, 0F,0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F); // Box 78
		bodyModel[28].setRotationPoint(2.5F, 6F, 8.5F);

		bodyModel[29].addShapeBox(0F, 0F, -2F, 5, 5, 0, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 89
		bodyModel[29].setRotationPoint(-11.5F, 5F, -3.5F);

		bodyModel[30].addShapeBox(0F, 0F, -2F, 5, 5, 0, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 90
		bodyModel[30].setRotationPoint(-11.5F, 5F, 7.5F);

		bodyModel[31].addShapeBox(0F, 0F, -2F, 5, 5, 0, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 91
		bodyModel[31].setRotationPoint(-2.5F, 5F, 7.5F);

		bodyModel[32].addShapeBox(0F, 0F, -2F, 5, 5, 0, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 92
		bodyModel[32].setRotationPoint(-2.5F, 5F, -3.5F);

		bodyModel[33].addShapeBox(0F, 0F, -2F, 5, 5, 0, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 93
		bodyModel[33].setRotationPoint(6.5F, 5F, 7.5F);

		bodyModel[34].addShapeBox(0F, 0F, -2F, 5, 5, 0, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 94
		bodyModel[34].setRotationPoint(6.5F, 5F, -3.5F);
	}
}