//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 05.11.2022 - 17:24:03
// Last changed on: 05.11.2022 - 17:24:03

package train.client.render.models; //Path where the model is located

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.common.api.AbstractTrains;
import train.common.library.Info;

public class ModelBTRSnowplow extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelBTRSnowplow() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[25];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 0
		bodyModel[1] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Box 1
		bodyModel[2] = new ModelRendererTurbo(this, 337, 1, textureX, textureY); // Box 2
		bodyModel[3] = new ModelRendererTurbo(this, 313, 1, textureX, textureY); // Box 3
		bodyModel[4] = new ModelRendererTurbo(this, 185, 25, textureX, textureY); // Box 11
		bodyModel[5] = new ModelRendererTurbo(this, 225, 25, textureX, textureY); // Box 12
		bodyModel[6] = new ModelRendererTurbo(this, 273, 25, textureX, textureY); // Box 13
		bodyModel[7] = new ModelRendererTurbo(this, 361, 25, textureX, textureY); // Box 14
		bodyModel[8] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Box 15
		bodyModel[9] = new ModelRendererTurbo(this, 65, 33, textureX, textureY); // Box 18
		bodyModel[10] = new ModelRendererTurbo(this, 297, 41, textureX, textureY); // Box 19
		bodyModel[11] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 20
		bodyModel[12] = new ModelRendererTurbo(this, 425, 25, textureX, textureY); // Box 22
		bodyModel[13] = new ModelRendererTurbo(this, 385, 41, textureX, textureY); // Box 23
		bodyModel[14] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 24
		bodyModel[15] = new ModelRendererTurbo(this, 81, 49, textureX, textureY); // Box 25
		bodyModel[16] = new ModelRendererTurbo(this, 481, 9, textureX, textureY); // Box 28
		bodyModel[17] = new ModelRendererTurbo(this, 169, 33, textureX, textureY); // Box 29
		bodyModel[18] = new ModelRendererTurbo(this, 249, 25, textureX, textureY); // Box 30
		bodyModel[19] = new ModelRendererTurbo(this, 185, 49, textureX, textureY); // Box 32
		bodyModel[20] = new ModelRendererTurbo(this, 481, 49, textureX, textureY); // Box 33
		bodyModel[21] = new ModelRendererTurbo(this, 153, 57, textureX, textureY); // Box 34
		bodyModel[22] = new ModelRendererTurbo(this, 497, 1, textureX, textureY); // Box 35
		bodyModel[23] = new ModelRendererTurbo(this, 153, 57, textureX, textureY); // Box 102
		bodyModel[24] = new ModelRendererTurbo(this, 481, 49, textureX, textureY); // Box 103

		bodyModel[0].addShapeBox(0F, 0F, 0F, 75, 2, 22, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 0
		bodyModel[0].setRotationPoint(-59F, 2F, -11F);

		bodyModel[1].addBox(0F, 0F, 0F, 75, 16, 1, 0F); // Box 1
		bodyModel[1].setRotationPoint(-59F, -14F, -11F);

		bodyModel[2].addBox(0F, 0F, 0F, 75, 16, 1, 0F); // Box 2
		bodyModel[2].setRotationPoint(-59F, -14F, 10F);

		bodyModel[3].addBox(0F, 0F, 0F, 1, 16, 20, 0F); // Box 3
		bodyModel[3].setRotationPoint(15F, -14F, -10F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 17, 2, 11, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -3F, 1F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 11
		bodyModel[4].setRotationPoint(-62F, -16F, -11F);

		bodyModel[5].addBox(0F, 0F, 0F, 1, 8, 20, 0F); // Box 12
		bodyModel[5].setRotationPoint(-10F, -22F, -10F);

		bodyModel[6].addBox(0F, 0F, 0F, 1, 8, 20, 0F); // Box 13
		bodyModel[6].setRotationPoint(-45F, -22F, -10F);

		bodyModel[7].addBox(0F, 0F, 0F, 36, 8, 1, 0F); // Box 14
		bodyModel[7].setRotationPoint(-45F, -22F, -11F);

		bodyModel[8].addBox(0F, 0F, 0F, 36, 8, 1, 0F); // Box 15
		bodyModel[8].setRotationPoint(-45F, -22F, 10F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 39, 2, 11, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -3F, 1F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 18
		bodyModel[9].setRotationPoint(-48F, -24F, -11F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 31, 3, 22, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, -2F, -3F, 0F, -2F); // Box 19
		bodyModel[10].setRotationPoint(-43F, 4F, -11F);

		bodyModel[11].addBox(0F, 0F, 0F, 4, 2, 2, 0F); // Box 20
		bodyModel[11].setRotationPoint(16F, 3F, -1F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 17, 2, 11, 0F,-3F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 22
		bodyModel[12].setRotationPoint(-62F, -16F, 0F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 39, 2, 11, 0F,-3F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 23
		bodyModel[13].setRotationPoint(-48F, -24F, 0F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 28, 2, 11, 0F,-3F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 24
		bodyModel[14].setRotationPoint(-12F, -16F, 0F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 28, 2, 11, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, -3F, 1F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F); // Box 25
		bodyModel[15].setRotationPoint(-12F, -16F, -11F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 1, 15, 11, 0F,3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 11F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 2F, 0F, 0F); // Box 28
		bodyModel[16].setRotationPoint(-60F, -10F, 0F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 1, 15, 11, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 11F, 0F, 0F); // Box 29
		bodyModel[17].setRotationPoint(-60F, -10F, -11F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F); // Box 30
		bodyModel[18].setRotationPoint(-60F, -16F, -11F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 1, 6, 11, 0F,4F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 32
		bodyModel[19].setRotationPoint(-60F, -16F, 0F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 33
		bodyModel[20].setRotationPoint(15F, 4F, -11F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 2, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 34
		bodyModel[21].setRotationPoint(15F, 4F, 1F);

		bodyModel[22].addBox(0F, 0F, 0F, 2, 6, 2, 0F); // Box 35
		bodyModel[22].setRotationPoint(-56F, -20F, -4F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 102
		bodyModel[23].setRotationPoint(16F, 3F, 1F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 103
		bodyModel[24].setRotationPoint(16F, 3F, -11F);
	}

	ModelTypeA theTrucks = new ModelTypeA();//textures/trains/typeA_Black.png
	Model70Truck theTrucks2 = new Model70Truck();

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		for (int i = 0; i < 25; i++) {
			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("lamp")) {
				Minecraft.getMinecraft().entityRenderer.disableLightmap(1D);
				bodyModel[i].render(f5);
				Minecraft.getMinecraft().entityRenderer.enableLightmap(1D);
			} else if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("cull")) {
				GL11.glDisable(GL11.GL_CULL_FACE);
				bodyModel[i].render(f5);
				GL11.glEnable(GL11.GL_CULL_FACE);
			} else {
				bodyModel[i].render(f5);
			}
		}
			Tessellator.bindTexture(new ResourceLocation(Info.resourceLocation, "textures/trains/70truck_Black.png"));
			GL11.glPushMatrix();
			GL11.glTranslated(-0.0F ,0.55F,-0.4F);
			theTrucks2.render(entity, f, f1, f2, f3, f4, f5);

			GL11.glTranslated(-3.25F,0.0F,0);
			theTrucks2.render(entity, f, f1, f2, f3, f4, f5);
			GL11.glPopMatrix();
		}
}