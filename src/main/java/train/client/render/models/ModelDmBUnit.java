//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2022 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 02.09.2021 - 12:49:00
// Last changed on: 02.09.2021 - 12:49:00

package train.client.render.models;


import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.core.ClientProxy;
import train.client.render.models.blocks.ModelLights;
import train.common.library.Info;
public class ModelDmBUnit extends ModelConverter //Same as Filename
{
	int textureX = 512;
	int textureY = 512;

	public ModelDmBUnit() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[73];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Du loco part01
		bodyModel[1] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Du loco part03
		bodyModel[2] = new ModelRendererTurbo(this, 313, 1, textureX, textureY); // Du loco part05
		bodyModel[3] = new ModelRendererTurbo(this, 313, 9, textureX, textureY); // Du loco part06
		bodyModel[4] = new ModelRendererTurbo(this, 337, 17, textureX, textureY); // Du loco part07
		bodyModel[5] = new ModelRendererTurbo(this, 1, 25, textureX, textureY); // Du loco part08
		bodyModel[6] = new ModelRendererTurbo(this, 457, 1, textureX, textureY); // Du loco part12
		bodyModel[7] = new ModelRendererTurbo(this, 481, 1, textureX, textureY); // Du loco part14
		bodyModel[8] = new ModelRendererTurbo(this, 481, 17, textureX, textureY); // Du loco part15
		bodyModel[9] = new ModelRendererTurbo(this, 153, 25, textureX, textureY); // Du loco part16
		bodyModel[10] = new ModelRendererTurbo(this, 177, 25, textureX, textureY); // Du loco part17
		bodyModel[11] = new ModelRendererTurbo(this, 313, 17, textureX, textureY); // Du loco part20
		bodyModel[12] = new ModelRendererTurbo(this, 297, 25, textureX, textureY); // Du loco part23
		bodyModel[13] = new ModelRendererTurbo(this, 1, 33, textureX, textureY); // Du loco part24
		bodyModel[14] = new ModelRendererTurbo(this, 25, 33, textureX, textureY); // Du loco part25
		bodyModel[15] = new ModelRendererTurbo(this, 49, 33, textureX, textureY); // Du loco part26
		bodyModel[16] = new ModelRendererTurbo(this, 177, 33, textureX, textureY); // Du loco part27
		bodyModel[17] = new ModelRendererTurbo(this, 73, 41, textureX, textureY); // Du loco part69
		bodyModel[18] = new ModelRendererTurbo(this, 361, 41, textureX, textureY); // Du loco part71
		bodyModel[19] = new ModelRendererTurbo(this, 1, 49, textureX, textureY); // Box 146
		bodyModel[20] = new ModelRendererTurbo(this, 145, 49, textureX, textureY); // Box 147
		bodyModel[21] = new ModelRendererTurbo(this, 361, 49, textureX, textureY); // Box 148
		bodyModel[22] = new ModelRendererTurbo(this, 1, 57, textureX, textureY); // Box 149
		bodyModel[23] = new ModelRendererTurbo(this, 145, 57, textureX, textureY); // Box 135
		bodyModel[24] = new ModelRendererTurbo(this, 289, 41, textureX, textureY); // Box 136
		bodyModel[25] = new ModelRendererTurbo(this, 289, 41, textureX, textureY); // Box 138
		bodyModel[26] = new ModelRendererTurbo(this, 337, 57, textureX, textureY); // Box 139
		bodyModel[27] = new ModelRendererTurbo(this, 369, 57, textureX, textureY); // Box 141
		bodyModel[28] = new ModelRendererTurbo(this, 313, 17, textureX, textureY); // Box 142
		bodyModel[29] = new ModelRendererTurbo(this, 73, 33, textureX, textureY); // Box 143
		bodyModel[30] = new ModelRendererTurbo(this, 401, 57, textureX, textureY); // Box 144
		bodyModel[31] = new ModelRendererTurbo(this, 417, 57, textureX, textureY); // Box 145
		bodyModel[32] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 146
		bodyModel[33] = new ModelRendererTurbo(this, 97, 33, textureX, textureY); // Box 147
		bodyModel[34] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 148
		bodyModel[35] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 149
		bodyModel[36] = new ModelRendererTurbo(this, 457, 57, textureX, textureY); // Box 150
		bodyModel[37] = new ModelRendererTurbo(this, 481, 57, textureX, textureY); // Box 151
		bodyModel[38] = new ModelRendererTurbo(this, 65, 65, textureX, textureY); // Box 152
		bodyModel[39] = new ModelRendererTurbo(this, 81, 65, textureX, textureY); // Box 153
		bodyModel[40] = new ModelRendererTurbo(this, 321, 25, textureX, textureY); // Box 154
		bodyModel[41] = new ModelRendererTurbo(this, 1, 65, textureX, textureY); // Box 155
		bodyModel[42] = new ModelRendererTurbo(this, 49, 65, textureX, textureY); // Box 156
		bodyModel[43] = new ModelRendererTurbo(this, 97, 65, textureX, textureY); // Box 157
		bodyModel[44] = new ModelRendererTurbo(this, 113, 33, textureX, textureY); // Box 158
		bodyModel[45] = new ModelRendererTurbo(this, 505, 1, textureX, textureY); // Box 159
		bodyModel[46] = new ModelRendererTurbo(this, 505, 9, textureX, textureY); // Box 160
		bodyModel[47] = new ModelRendererTurbo(this, 113, 65, textureX, textureY); // Du loco part16
		bodyModel[48] = new ModelRendererTurbo(this, 337, 73, textureX, textureY); // Du loco part26
		bodyModel[49] = new ModelRendererTurbo(this, 89, 81, textureX, textureY); // Box 146
		bodyModel[50] = new ModelRendererTurbo(this, 1, 89, textureX, textureY); // Box 146
		bodyModel[51] = new ModelRendererTurbo(this, 225, 41, textureX, textureY); // Box 149
		bodyModel[52] = new ModelRendererTurbo(this, 361, 73, textureX, textureY); // Box 149
		bodyModel[53] = new ModelRendererTurbo(this, 473, 73, textureX, textureY); // Box 149
		bodyModel[54] = new ModelRendererTurbo(this, 161, 9, textureX, textureY); // Du loco part37
		bodyModel[55] = new ModelRendererTurbo(this, 505, 17, textureX, textureY); // Du loco part37
		bodyModel[56] = new ModelRendererTurbo(this, 505, 25, textureX, textureY); // Du loco part37
		bodyModel[57] = new ModelRendererTurbo(this, 145, 89, textureX, textureY); // Du loco part37
		bodyModel[58] = new ModelRendererTurbo(this, 1, 81, textureX, textureY); // Box 149
		bodyModel[59] = new ModelRendererTurbo(this, 241, 81, textureX, textureY); // Box 149
		bodyModel[60] = new ModelRendererTurbo(this, 281, 81, textureX, textureY); // Box 149
		bodyModel[61] = new ModelRendererTurbo(this, 73, 33, textureX, textureY); // Du loco part37
		bodyModel[62] = new ModelRendererTurbo(this, 313, 81, textureX, textureY); // Box 139
		bodyModel[63] = new ModelRendererTurbo(this, 353, 81, textureX, textureY); // Box 141
		bodyModel[64] = new ModelRendererTurbo(this, 129, 33, textureX, textureY); // Box 142
		bodyModel[65] = new ModelRendererTurbo(this, 481, 33, textureX, textureY); // Box 143
		bodyModel[66] = new ModelRendererTurbo(this, 385, 81, textureX, textureY); // Box 144
		bodyModel[67] = new ModelRendererTurbo(this, 457, 81, textureX, textureY); // Box 145
		bodyModel[68] = new ModelRendererTurbo(this, 1, 97, textureX, textureY); // Box 146
		bodyModel[69] = new ModelRendererTurbo(this, 257, 41, textureX, textureY); // Box 147
		bodyModel[70] = new ModelRendererTurbo(this, 505, 49, textureX, textureY); // Box 148
		bodyModel[71] = new ModelRendererTurbo(this, 145, 33, textureX, textureY); // Box 149
		bodyModel[72] = new ModelRendererTurbo(this, 289, 89, textureX, textureY); // Box 138

		bodyModel[0].addBox(0F, 0F, 0F, 70, 8, 12, 0F); // Du loco part01
		bodyModel[0].setRotationPoint(14F, -11F, -6F);

		bodyModel[1].addBox(0F, 0F, 0F, 70, 1, 22, 0F); // Du loco part03
		bodyModel[1].setRotationPoint(14F, -12F, -11F);

		bodyModel[2].addBox(0F, 0F, 0F, 70, 1, 1, 0F); // Du loco part05
		bodyModel[2].setRotationPoint(14F, -11F, 7F);

		bodyModel[3].addBox(0F, 0F, 0F, 70, 1, 1, 0F); // Du loco part06
		bodyModel[3].setRotationPoint(14F, -11F, -8F);

		bodyModel[4].addBox(0F, 0F, 0F, 70, 18, 1, 0F); // Du loco part07
		bodyModel[4].setRotationPoint(14F, -30F, -11F);

		bodyModel[5].addBox(0F, 0F, 0F, 70, 3, 4, 0F); // Du loco part08
		bodyModel[5].setRotationPoint(14F, -33F, -2F);

		bodyModel[6].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part12
		bodyModel[6].setRotationPoint(16F, -11F, -6.05F);

		bodyModel[7].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part14
		bodyModel[7].setRotationPoint(30F, -11F, -6.05F);

		bodyModel[8].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part15
		bodyModel[8].setRotationPoint(43F, -11F, -6.05F);

		bodyModel[9].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part16
		bodyModel[9].setRotationPoint(58F, -11F, -6.05F);

		bodyModel[10].addBox(0F, 0F, 0F, 57, 1, 0, 0F); // Du loco part17
		bodyModel[10].setRotationPoint(20F, -9F, -6.1F);

		bodyModel[11].addBox(0F, 0F, 0F, 1, 18, 20, 0F); // Du loco part20
		bodyModel[11].setRotationPoint(83F, -30F, -10F);

		bodyModel[12].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part23
		bodyModel[12].setRotationPoint(16F, -11F, 6.05F);

		bodyModel[13].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part24
		bodyModel[13].setRotationPoint(30F, -11F, 6.05F);

		bodyModel[14].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part25
		bodyModel[14].setRotationPoint(43F, -11F, 6.05F);

		bodyModel[15].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part26
		bodyModel[15].setRotationPoint(58F, -11F, 6.05F);

		bodyModel[16].addBox(0F, 0F, 0F, 57, 1, 0, 0F); // Du loco part27
		bodyModel[16].setRotationPoint(20F, -9F, 6.1F);

		bodyModel[17].addBox(0F, 0F, 0F, 70, 2, 4, 0F); // Du loco part69
		bodyModel[17].setRotationPoint(14F, -32F, 2F);

		bodyModel[18].addBox(0F, 0F, 0F, 70, 2, 4, 0F); // Du loco part71
		bodyModel[18].setRotationPoint(14F, -32F, -6F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 70, 1, 1, 0F,0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, -1F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F); // Box 146
		bodyModel[19].setRotationPoint(14F, -31F, 6F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 70, 1, 1, 0F,0F, 1F, -1F, 0F, 1F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 4F, 0F, 0F, 4F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 147
		bodyModel[20].setRotationPoint(14F, -31F, -7F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 70, 1, 1, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 148
		bodyModel[21].setRotationPoint(14F, -33F, -3F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 70, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 3F, 0F, 0F, 3F); // Box 149
		bodyModel[22].setRotationPoint(14F, -33F, 2F);

		bodyModel[23].addBox(0F, 0F, 0F, 70, 18, 1, 0F); // Box 135
		bodyModel[23].setRotationPoint(14F, -30F, 10F);

		bodyModel[24].addBox(0F, 0F, 0F, 1, 18, 20, 0F); // Box 136
		bodyModel[24].setRotationPoint(14F, -30F, -10F);

		bodyModel[25].addBox(0F, 0F, 0F, 4, 4, 4, 0F); // Box 138
		bodyModel[25].setRotationPoint(20F, -16F, -2F);

		bodyModel[26].addBox(0F, 0F, 0F, 6, 6, 6, 0F); // Box 139
		bodyModel[26].setRotationPoint(19F, -22F, -3F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 141
		bodyModel[27].setRotationPoint(19F, -24F, -3F);

		bodyModel[28].addBox(0F, 0F, 0F, 4, 1, 4, 0F); // Box 142
		bodyModel[28].setRotationPoint(20F, -25F, -2F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 6, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 143
		bodyModel[29].setRotationPoint(19F, -26F, -3F);

		bodyModel[30].addBox(0F, 0F, 0F, 6, 4, 6, 0F); // Box 144
		bodyModel[30].setRotationPoint(19F, -30F, -3F);

		bodyModel[31].addBox(0F, 0F, 0F, 12, 15, 12, 0F); // Box 145
		bodyModel[31].setRotationPoint(28F, -27F, -5F);

		bodyModel[32].addBox(0F, 0F, 0F, 14, 1, 14, 0F); // Box 146
		bodyModel[32].setRotationPoint(27F, -28F, -6F);

		bodyModel[33].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 147
		bodyModel[33].setRotationPoint(22F, -19F, -4F);

		bodyModel[34].addBox(0F, 0F, 0F, 1, 7, 1, 0F); // Box 148
		bodyModel[34].setRotationPoint(26F, -26F, -4F);

		bodyModel[35].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 149
		bodyModel[35].setRotationPoint(27F, -26F, -4F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 150
		bodyModel[36].setRotationPoint(41F, -16F, -2F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 4, 4, 4, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 151
		bodyModel[37].setRotationPoint(51F, -16F, -2F);

		bodyModel[38].addBox(0F, 0F, 0F, 2, 14, 2, 0F); // Box 152
		bodyModel[38].setRotationPoint(52F, -30F, -1F);

		bodyModel[39].addBox(0F, 0F, 0F, 2, 14, 2, 0F); // Box 153
		bodyModel[39].setRotationPoint(42F, -30F, -1F);

		bodyModel[40].addBox(0F, 0F, 0F, 2, 9, 2, 0F); // Box 154
		bodyModel[40].setRotationPoint(45F, -27F, -1F);

		bodyModel[41].addBox(0F, 0F, 0F, 2, 9, 2, 0F); // Box 155
		bodyModel[41].setRotationPoint(49F, -27F, -1F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 1F, 1F, 0F, 1F); // Box 156
		bodyModel[42].setRotationPoint(49F, -18F, -1F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 2, 6, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 1F, 0F, 1F, 1F, 0F, 1F, 0F, 0F, 1F); // Box 157
		bodyModel[43].setRotationPoint(45F, -18F, -1F);

		bodyModel[44].addBox(0F, 0F, 0F, 2, 2, 2, 0F); // Box 158
		bodyModel[44].setRotationPoint(47F, -26F, -1F);

		bodyModel[45].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 159
		bodyModel[45].setRotationPoint(51F, -26F, -1F);

		bodyModel[46].addBox(0F, 0F, 0F, 1, 2, 2, 0F); // Box 160
		bodyModel[46].setRotationPoint(44F, -26F, -1F);

		bodyModel[47].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part16
		bodyModel[47].setRotationPoint(72F, -11F, -6.05F);

		bodyModel[48].addBox(0F, 0F, 0F, 10, 10, 0, 0F); // Du loco part26
		bodyModel[48].setRotationPoint(72F, -11F, 6.05F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 70, 2, 2, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[49].setRotationPoint(14F, -32F, -11F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 70, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 146
		bodyModel[50].setRotationPoint(14F, -32F, 9F);

		bodyModel[51].addBox(0F, 0F, 0F, 14, 1, 3, 0F); // Box 149
		bodyModel[51].setRotationPoint(70F, -34F, -5F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 14, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 149
		bodyModel[52].setRotationPoint(70F, -35F, -5F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 14, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 149
		bodyModel[53].setRotationPoint(70F, -33F, -5F);

		bodyModel[54].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Du loco part37
		bodyModel[54].setRotationPoint(37F, -34F, -6F);

		bodyModel[55].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Du loco part37
		bodyModel[55].setRotationPoint(56F, -34F, -6F);

		bodyModel[56].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Du loco part37
		bodyModel[56].setRotationPoint(76F, -34F, -6F);

		bodyModel[57].addBox(0F, 0F, 0F, 70, 1, 0, 0F); // Du loco part37
		bodyModel[57].setRotationPoint(14F, -34F, -6F);

		bodyModel[58].addBox(0F, 0F, 0F, 14, 1, 3, 0F); // Box 149
		bodyModel[58].setRotationPoint(14F, -34F, 2F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 14, 1, 3, 0F,0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 149
		bodyModel[59].setRotationPoint(14F, -35F, 2F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 14, 1, 3, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, 0F, -1F); // Box 149
		bodyModel[60].setRotationPoint(14F, -33F, 2F);

		bodyModel[61].addBox(0F, 0F, 0F, 1, 2, 1, 0F); // Du loco part37
		bodyModel[61].setRotationPoint(20F, -34F, -6F);

		bodyModel[62].addBox(0F, 0F, 0F, 6, 6, 6, 0F); // Box 139
		bodyModel[62].setRotationPoint(71F, -22F, -3F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 6, 2, 6, 0F,-1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 141
		bodyModel[63].setRotationPoint(71F, -24F, -3F);

		bodyModel[64].addBox(0F, 0F, 0F, 4, 1, 4, 0F); // Box 142
		bodyModel[64].setRotationPoint(72F, -25F, -2F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 6, 1, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F, -1F, 0F, -1F); // Box 143
		bodyModel[65].setRotationPoint(71F, -26F, -3F);

		bodyModel[66].addBox(0F, 0F, 0F, 6, 4, 6, 0F); // Box 144
		bodyModel[66].setRotationPoint(71F, -30F, -3F);

		bodyModel[67].addBox(0F, 0F, 0F, 12, 15, 12, 0F); // Box 145
		bodyModel[67].setRotationPoint(56F, -27F, -5F);

		bodyModel[68].addBox(0F, 0F, 0F, 14, 1, 14, 0F); // Box 146
		bodyModel[68].setRotationPoint(55F, -28F, -6F);

		bodyModel[69].addBox(0F, 0F, 0F, 5, 1, 1, 0F); // Box 147
		bodyModel[69].setRotationPoint(69F, -19F, -4F);

		bodyModel[70].addBox(0F, 0F, 0F, 1, 7, 1, 0F); // Box 148
		bodyModel[70].setRotationPoint(69F, -26F, -4F);

		bodyModel[71].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 149
		bodyModel[71].setRotationPoint(68F, -26F, -4F);

		bodyModel[72].addBox(0F, 0F, 0F, 4, 4, 4, 0F); // Box 138
		bodyModel[72].setRotationPoint(72F, -16F, -2F);
	}
}