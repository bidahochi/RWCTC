//FMT-Marker DFM-1.0
//Creator: broscolotos

package train.client.render.models;


import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import tmt.Tessellator;
import train.client.core.ClientProxy;
import train.client.render.models.blocks.ModelLights;
import train.common.library.Info;

/** This file was exported via the (Default) FlansMod Exporter of<br>
 *  FMT (Fex's Modelling Toolbox) v.2.6.6 &copy; 2021 - Fexcraft.net<br>
 *  All rights reserved. For this Model's License contact the Author/Creator.
 */
public class HHeavyweight extends ModelConverter {

	private int textureX = 256;
	private int textureY = 256;
	private ModelLights lights;

	public HHeavyweight(){
		bodyModel = new ModelRendererTurbo[231];
		lights = new ModelLights();
		//
		bodyModel[0] = new ModelRendererTurbo(this, 68, 76, textureX, textureY);
		bodyModel[0].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[0].setRotationPoint(-13.0f, 7.5f, -6.0f);

		bodyModel[1] = new ModelRendererTurbo(this, 135, 70, textureX, textureY);
		bodyModel[1].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[1].setRotationPoint(-29.0f, 7.5f, -6.0f);

		bodyModel[2] = new ModelRendererTurbo(this, 68, 70, textureX, textureY);
		bodyModel[2].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[2].setRotationPoint(-13.0f, 7.5f, 6.0f);

		bodyModel[3] = new ModelRendererTurbo(this, 150, 62, textureX, textureY);
		bodyModel[3].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[3].setRotationPoint(-29.0f, 7.5f, 6.0f);

		bodyModel[4] = new ModelRendererTurbo(this, 0, 117, textureX, textureY);
		bodyModel[4].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 16, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0.5f, 0.5f, 0, 0.5f, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[4].setRotationPoint(-27.25f, 9.25f, -8.0f);

		bodyModel[5] = new ModelRendererTurbo(this, 0, 30, textureX, textureY);
		bodyModel[5].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[5].setRotationPoint(-21.0f, 7.5f, -6.0f);

		bodyModel[6] = new ModelRendererTurbo(this, 0, 24, textureX, textureY);
		bodyModel[6].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[6].setRotationPoint(-21.0f, 7.5f, 6.0f);

		bodyModel[7] = new ModelRendererTurbo(this, 213, 111, textureX, textureY);
		bodyModel[7].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 16, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0.5f, 0.5f, 0, 0.5f, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[7].setRotationPoint(-19.25f, 9.25f, -8.0f);

		bodyModel[8] = new ModelRendererTurbo(this, 57, 109, textureX, textureY);
		bodyModel[8].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 16, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0.5f, 0.5f, 0, 0.5f, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[8].setRotationPoint(-11.25f, 9.25f, -8.0f);

		bodyModel[9] = new ModelRendererTurbo(this, 107, 49, textureX, textureY);
		bodyModel[9].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[9].setRotationPoint(-12.5f, 8.5f, 7.0f);

		bodyModel[10] = new ModelRendererTurbo(this, 107, 45, textureX, textureY);
		bodyModel[10].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[10].setRotationPoint(-28.5f, 8.5f, 7.0f);

		bodyModel[11] = new ModelRendererTurbo(this, 13, 17, textureX, textureY);
		bodyModel[11].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[11].setRotationPoint(-20.5f, 8.5f, 7.0f);

		bodyModel[12] = new ModelRendererTurbo(this, 13, 13, textureX, textureY);
		bodyModel[12].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[12].setRotationPoint(-12.5f, 8.5f, -7.0f);

		bodyModel[13] = new ModelRendererTurbo(this, 13, 4, textureX, textureY);
		bodyModel[13].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[13].setRotationPoint(-28.5f, 8.5f, -7.0f);

		bodyModel[14] = new ModelRendererTurbo(this, 13, 0, textureX, textureY);
		bodyModel[14].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[14].setRotationPoint(-20.5f, 8.5f, -7.0f);

		bodyModel[15] = new ModelRendererTurbo(this, 0, 66, textureX, textureY);
		bodyModel[15].addShapeBox(-2.5f, -2.5f, 0, 23, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[15].setRotationPoint(-30.0f, 7.5f, 6.0f);

		bodyModel[16] = new ModelRendererTurbo(this, 205, 62, textureX, textureY);
		bodyModel[16].addShapeBox(-2.5f, -2.5f, 0, 23, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[16].setRotationPoint(-30.0f, 7.5f, -7.0f);

		bodyModel[17] = new ModelRendererTurbo(this, 191, 144, textureX, textureY);
		bodyModel[17].addShapeBox(-2.5f, -2.5f, 0, 1, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[17].setRotationPoint(-8.0f, 7.5f, -6.0f);

		bodyModel[18] = new ModelRendererTurbo(this, 68, 70, textureX, textureY);
		bodyModel[18].addShapeBox(-2.5f, -2.5f, 0, 1, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[18].setRotationPoint(-30.0f, 7.5f, -6.0f);

		bodyModel[19] = new ModelRendererTurbo(this, 0, 8, textureX, textureY);
		bodyModel[19].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[19].setRotationPoint(-23.5f, 11.0f, 7.0f);

		bodyModel[20] = new ModelRendererTurbo(this, 250, 6, textureX, textureY);
		bodyModel[20].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[20].setRotationPoint(-23.5f, 11.0f, -7.0f);

		bodyModel[21] = new ModelRendererTurbo(this, 237, 58, textureX, textureY);
		bodyModel[21].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[21].setRotationPoint(-29.0f, 8.25f, -8.0f);

		bodyModel[22] = new ModelRendererTurbo(this, 224, 58, textureX, textureY);
		bodyModel[22].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[22].setRotationPoint(-29.0f, 8.25f, 7.0f);

		bodyModel[23] = new ModelRendererTurbo(this, 224, 50, textureX, textureY);
		bodyModel[23].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[23].setRotationPoint(-21.0f, 8.25f, -8.0f);

		bodyModel[24] = new ModelRendererTurbo(this, 224, 47, textureX, textureY);
		bodyModel[24].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[24].setRotationPoint(-21.0f, 8.25f, 7.0f);

		bodyModel[25] = new ModelRendererTurbo(this, 224, 44, textureX, textureY);
		bodyModel[25].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[25].setRotationPoint(-13.0f, 8.25f, -8.0f);

		bodyModel[26] = new ModelRendererTurbo(this, 224, 41, textureX, textureY);
		bodyModel[26].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[26].setRotationPoint(-13.0f, 8.25f, 7.0f);

		bodyModel[27] = new ModelRendererTurbo(this, 246, 30, textureX, textureY);
		bodyModel[27].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[27].setRotationPoint(-24.5f, 10.75f, 7.0f);

		bodyModel[28] = new ModelRendererTurbo(this, 244, 24, textureX, textureY);
		bodyModel[28].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[28].setRotationPoint(-24.5f, 10.75f, -7.0f);

		bodyModel[29] = new ModelRendererTurbo(this, 127, 8, textureX, textureY);
		bodyModel[29].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[29].setRotationPoint(-21.5f, 10.75f, 7.0f);

		bodyModel[30] = new ModelRendererTurbo(this, 18, 8, textureX, textureY);
		bodyModel[30].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[30].setRotationPoint(-21.5f, 10.75f, -7.0f);

		bodyModel[31] = new ModelRendererTurbo(this, 250, 2, textureX, textureY);
		bodyModel[31].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[31].setRotationPoint(-15.5f, 11.0f, 7.0f);

		bodyModel[32] = new ModelRendererTurbo(this, 250, 0, textureX, textureY);
		bodyModel[32].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[32].setRotationPoint(-15.5f, 11.0f, -7.0f);

		bodyModel[33] = new ModelRendererTurbo(this, 5, 8, textureX, textureY);
		bodyModel[33].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[33].setRotationPoint(-16.5f, 10.75f, 7.0f);

		bodyModel[34] = new ModelRendererTurbo(this, 127, 6, textureX, textureY);
		bodyModel[34].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[34].setRotationPoint(-16.5f, 10.75f, -7.0f);

		bodyModel[35] = new ModelRendererTurbo(this, 127, 2, textureX, textureY);
		bodyModel[35].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[35].setRotationPoint(-13.5f, 10.75f, 7.0f);

		bodyModel[36] = new ModelRendererTurbo(this, 127, 0, textureX, textureY);
		bodyModel[36].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[36].setRotationPoint(-13.5f, 10.75f, -7.0f);

		bodyModel[37] = new ModelRendererTurbo(this, 221, 129, textureX, textureY);
		bodyModel[37].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[37].setRotationPoint(-23.5f, 8.75f, -7.0f);

		bodyModel[38] = new ModelRendererTurbo(this, 188, 127, textureX, textureY);
		bodyModel[38].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[38].setRotationPoint(-15.5f, 8.75f, -7.0f);

		bodyModel[39] = new ModelRendererTurbo(this, 0, 17, textureX, textureY);
		bodyModel[39].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0);
		bodyModel[39].setRotationPoint(-23.5f, 8.75f, -8.0f);

		bodyModel[40] = new ModelRendererTurbo(this, 0, 13, textureX, textureY);
		bodyModel[40].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0);
		bodyModel[40].setRotationPoint(-15.5f, 8.75f, -8.0f);

		bodyModel[41] = new ModelRendererTurbo(this, 0, 4, textureX, textureY);
		bodyModel[41].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1);
		bodyModel[41].setRotationPoint(-23.5f, 8.75f, 7.0f);

		bodyModel[42] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[42].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1);
		bodyModel[42].setRotationPoint(-15.5f, 8.75f, 7.0f);

		bodyModel[43] = new ModelRendererTurbo(this, 101, 62, textureX, textureY);
		bodyModel[43].addShapeBox(-2.5f, -2.5f, 0, 21, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[43].setRotationPoint(-29.0f, 7.75f, -3.0f);

		bodyModel[44] = new ModelRendererTurbo(this, 161, 70, textureX, textureY);
		bodyModel[44].addShapeBox(-2.5f, -2.5f, 0, 4, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[44].setRotationPoint(-20.5f, 6.75f, -2.0f);

		bodyModel[45] = new ModelRendererTurbo(this, 25, 88, textureX, textureY);
		bodyModel[45].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[45].setRotationPoint(20.0f, 7.5f, -6.0f);

		bodyModel[46] = new ModelRendererTurbo(this, 11, 87, textureX, textureY);
		bodyModel[46].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[46].setRotationPoint(4.0f, 7.5f, -6.0f);

		bodyModel[47] = new ModelRendererTurbo(this, 0, 87, textureX, textureY);
		bodyModel[47].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[47].setRotationPoint(20.0f, 7.5f, 6.0f);

		bodyModel[48] = new ModelRendererTurbo(this, 207, 84, textureX, textureY);
		bodyModel[48].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[48].setRotationPoint(4.0f, 7.5f, 6.0f);

		bodyModel[49] = new ModelRendererTurbo(this, 38, 124, textureX, textureY);
		bodyModel[49].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 16, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0.5f, 0.5f, 0, 0.5f, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[49].setRotationPoint(5.75f, 9.25f, -8.0f);

		bodyModel[50] = new ModelRendererTurbo(this, 159, 83, textureX, textureY);
		bodyModel[50].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[50].setRotationPoint(12.0f, 7.5f, -6.0f);

		bodyModel[51] = new ModelRendererTurbo(this, 148, 83, textureX, textureY);
		bodyModel[51].addShapeBox(-2.5f, -2.5f, 0, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[51].setRotationPoint(12.0f, 7.5f, 6.0f);

		bodyModel[52] = new ModelRendererTurbo(this, 19, 122, textureX, textureY);
		bodyModel[52].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 16, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0.5f, 0.5f, 0, 0.5f, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[52].setRotationPoint(13.75f, 9.25f, -8.0f);

		bodyModel[53] = new ModelRendererTurbo(this, 76, 119, textureX, textureY);
		bodyModel[53].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 16, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0.5f, 0.5f, 0, 0.5f, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[53].setRotationPoint(21.75f, 9.25f, -8.0f);

		bodyModel[54] = new ModelRendererTurbo(this, 207, 92, textureX, textureY);
		bodyModel[54].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[54].setRotationPoint(20.5f, 8.5f, 7.0f);

		bodyModel[55] = new ModelRendererTurbo(this, 75, 90, textureX, textureY);
		bodyModel[55].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[55].setRotationPoint(4.5f, 8.5f, 7.0f);

		bodyModel[56] = new ModelRendererTurbo(this, 244, 89, textureX, textureY);
		bodyModel[56].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[56].setRotationPoint(12.5f, 8.5f, 7.0f);

		bodyModel[57] = new ModelRendererTurbo(this, 235, 89, textureX, textureY);
		bodyModel[57].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[57].setRotationPoint(20.5f, 8.5f, -7.0f);

		bodyModel[58] = new ModelRendererTurbo(this, 107, 57, textureX, textureY);
		bodyModel[58].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[58].setRotationPoint(4.5f, 8.5f, -7.0f);

		bodyModel[59] = new ModelRendererTurbo(this, 107, 53, textureX, textureY);
		bodyModel[59].addShapeBox(-2.5f, -2.5f, 0, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[59].setRotationPoint(12.5f, 8.5f, -7.0f);

		bodyModel[60] = new ModelRendererTurbo(this, 0, 70, textureX, textureY);
		bodyModel[60].addShapeBox(-2.5f, -2.5f, 0, 23, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[60].setRotationPoint(3.0f, 7.5f, 6.0f);

		bodyModel[61] = new ModelRendererTurbo(this, 49, 66, textureX, textureY);
		bodyModel[61].addShapeBox(-2.5f, -2.5f, 0, 23, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[61].setRotationPoint(3.0f, 7.5f, -7.0f);

		bodyModel[62] = new ModelRendererTurbo(this, 0, 152, textureX, textureY);
		bodyModel[62].addShapeBox(-2.5f, -2.5f, 0, 1, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[62].setRotationPoint(25.0f, 7.5f, -6.0f);

		bodyModel[63] = new ModelRendererTurbo(this, 155, 150, textureX, textureY);
		bodyModel[63].addShapeBox(-2.5f, -2.5f, 0, 1, 2, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[63].setRotationPoint(3.0f, 7.5f, -6.0f);

		bodyModel[64] = new ModelRendererTurbo(this, 251, 26, textureX, textureY);
		bodyModel[64].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[64].setRotationPoint(9.5f, 11.0f, 7.0f);

		bodyModel[65] = new ModelRendererTurbo(this, 251, 24, textureX, textureY);
		bodyModel[65].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[65].setRotationPoint(9.5f, 11.0f, -7.0f);

		bodyModel[66] = new ModelRendererTurbo(this, 161, 76, textureX, textureY);
		bodyModel[66].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[66].setRotationPoint(4.0f, 8.25f, -8.0f);

		bodyModel[67] = new ModelRendererTurbo(this, 230, 75, textureX, textureY);
		bodyModel[67].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[67].setRotationPoint(4.0f, 8.25f, 7.0f);

		bodyModel[68] = new ModelRendererTurbo(this, 198, 75, textureX, textureY);
		bodyModel[68].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[68].setRotationPoint(12.0f, 8.25f, -8.0f);

		bodyModel[69] = new ModelRendererTurbo(this, 117, 75, textureX, textureY);
		bodyModel[69].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[69].setRotationPoint(12.0f, 8.25f, 7.0f);

		bodyModel[70] = new ModelRendererTurbo(this, 83, 75, textureX, textureY);
		bodyModel[70].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[70].setRotationPoint(20.0f, 8.25f, -8.0f);

		bodyModel[71] = new ModelRendererTurbo(this, 195, 70, textureX, textureY);
		bodyModel[71].addShapeBox(-2.5f, -2.5f, 0, 5, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[71].setRotationPoint(20.0f, 8.25f, 7.0f);

		bodyModel[72] = new ModelRendererTurbo(this, 208, 66, textureX, textureY);
		bodyModel[72].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[72].setRotationPoint(8.5f, 10.75f, 7.0f);

		bodyModel[73] = new ModelRendererTurbo(this, 205, 66, textureX, textureY);
		bodyModel[73].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[73].setRotationPoint(8.5f, 10.75f, -7.0f);

		bodyModel[74] = new ModelRendererTurbo(this, 104, 66, textureX, textureY);
		bodyModel[74].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[74].setRotationPoint(11.5f, 10.75f, 7.0f);

		bodyModel[75] = new ModelRendererTurbo(this, 101, 66, textureX, textureY);
		bodyModel[75].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[75].setRotationPoint(11.5f, 10.75f, -7.0f);

		bodyModel[76] = new ModelRendererTurbo(this, 250, 8, textureX, textureY);
		bodyModel[76].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[76].setRotationPoint(17.5f, 11.0f, 7.0f);

		bodyModel[77] = new ModelRendererTurbo(this, 13, 8, textureX, textureY);
		bodyModel[77].addShapeBox(-2.5f, -2.5f, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[77].setRotationPoint(17.5f, 11.0f, -7.0f);

		bodyModel[78] = new ModelRendererTurbo(this, 98, 66, textureX, textureY);
		bodyModel[78].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[78].setRotationPoint(16.5f, 10.75f, 7.0f);

		bodyModel[79] = new ModelRendererTurbo(this, 253, 61, textureX, textureY);
		bodyModel[79].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0);
		bodyModel[79].setRotationPoint(16.5f, 10.75f, -7.0f);

		bodyModel[80] = new ModelRendererTurbo(this, 252, 30, textureX, textureY);
		bodyModel[80].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[80].setRotationPoint(19.5f, 10.75f, 7.0f);

		bodyModel[81] = new ModelRendererTurbo(this, 249, 30, textureX, textureY);
		bodyModel[81].addShapeBox(-2.5f, -2.5f, 0, 1, 1, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0);
		bodyModel[81].setRotationPoint(19.5f, 10.75f, -7.0f);

		bodyModel[82] = new ModelRendererTurbo(this, 97, 135, textureX, textureY);
		bodyModel[82].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[82].setRotationPoint(9.5f, 8.75f, -7.0f);

		bodyModel[83] = new ModelRendererTurbo(this, 0, 135, textureX, textureY);
		bodyModel[83].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[83].setRotationPoint(17.5f, 8.75f, -7.0f);

		bodyModel[84] = new ModelRendererTurbo(this, 182, 80, textureX, textureY);
		bodyModel[84].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0);
		bodyModel[84].setRotationPoint(9.5f, 8.75f, -8.0f);

		bodyModel[85] = new ModelRendererTurbo(this, 248, 79, textureX, textureY);
		bodyModel[85].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0);
		bodyModel[85].setRotationPoint(17.5f, 8.75f, -8.0f);

		bodyModel[86] = new ModelRendererTurbo(this, 182, 76, textureX, textureY);
		bodyModel[86].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1);
		bodyModel[86].setRotationPoint(9.5f, 8.75f, 7.0f);

		bodyModel[87] = new ModelRendererTurbo(this, 248, 75, textureX, textureY);
		bodyModel[87].addShapeBox(-2.5f, -2.5f, 0, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.5f, 0, -0.5f, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1);
		bodyModel[87].setRotationPoint(17.5f, 8.75f, 7.0f);

		bodyModel[88] = new ModelRendererTurbo(this, 156, 62, textureX, textureY);
		bodyModel[88].addShapeBox(-2.5f, -2.5f, 0, 21, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[88].setRotationPoint(4.0f, 7.75f, -3.0f);

		bodyModel[89] = new ModelRendererTurbo(this, 182, 70, textureX, textureY);
		bodyModel[89].addShapeBox(-2.5f, -2.5f, 0, 4, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[89].setRotationPoint(12.5f, 6.75f, -2.0f);

		bodyModel[90] = new ModelRendererTurbo(this, 230, 66, textureX, textureY);
		bodyModel[90].addShapeBox(0, 0, 0, 8, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[90].setRotationPoint(-8.5f, 4.25f, -10.0f);

		bodyModel[91] = new ModelRendererTurbo(this, 125, 94, textureX, textureY);
		bodyModel[91].addShapeBox(0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25f, 0, 0, 0.25f, 0, 0, 0.25f, 0, 0, 0.25f, 0);
		bodyModel[91].setRotationPoint(-8.5f, 4.25f, 7.5f);

		bodyModel[92] = new ModelRendererTurbo(this, 0, 74, textureX, textureY);
		bodyModel[92].addShapeBox(0, 0, 0, 5, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[92].setRotationPoint(-5.5f, 4.25f, 7.0f);

		bodyModel[93] = new ModelRendererTurbo(this, 75, 88, textureX, textureY);
		bodyModel[93].addShapeBox(0, 0, 0, 9, 2, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[93].setRotationPoint(-9.0f, 4.25f, -6.0f);

		bodyModel[94] = new ModelRendererTurbo(this, 200, 94, textureX, textureY);
		bodyModel[94].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[94].setRotationPoint(-35.0f, 4.25f, -10.5f);

		bodyModel[95] = new ModelRendererTurbo(this, 197, 94, textureX, textureY);
		bodyModel[95].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0);
		bodyModel[95].setRotationPoint(-35.0f, 4.25f, -11.5f);

		bodyModel[96] = new ModelRendererTurbo(this, 194, 94, textureX, textureY);
		bodyModel[96].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[96].setRotationPoint(-30.5f, 4.25f, -10.5f);

		bodyModel[97] = new ModelRendererTurbo(this, 191, 94, textureX, textureY);
		bodyModel[97].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0);
		bodyModel[97].setRotationPoint(-30.5f, 4.25f, -11.5f);

		bodyModel[98] = new ModelRendererTurbo(this, 0, 81, textureX, textureY);
		bodyModel[98].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[98].setRotationPoint(-35.0f, 7.25f, -11.25f);

		bodyModel[99] = new ModelRendererTurbo(this, 146, 89, textureX, textureY);
		bodyModel[99].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[99].setRotationPoint(-35.0f, 6.25f, -10.25f);

		bodyModel[100] = new ModelRendererTurbo(this, 83, 80, textureX, textureY);
		bodyModel[100].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[100].setRotationPoint(-35.0f, 6.25f, -10.75f);

		bodyModel[101] = new ModelRendererTurbo(this, 135, 89, textureX, textureY);
		bodyModel[101].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[101].setRotationPoint(-35.0f, 5.25f, -10.0f);

		bodyModel[102] = new ModelRendererTurbo(this, 161, 79, textureX, textureY);
		bodyModel[102].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[102].setRotationPoint(-35.0f, 5.25f, -10.5f);

		bodyModel[103] = new ModelRendererTurbo(this, 119, 88, textureX, textureY);
		bodyModel[103].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[103].setRotationPoint(-35.0f, 4.25f, -9.75f);

		bodyModel[104] = new ModelRendererTurbo(this, 230, 78, textureX, textureY);
		bodyModel[104].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[104].setRotationPoint(-35.0f, 4.25f, -10.5f);

		bodyModel[105] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[105].addShapeBox(0, 0, 0, 52, 1, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[105].setRotationPoint(-30.5f, 3.25f, -11.0f);

		bodyModel[106] = new ModelRendererTurbo(this, 47, 88, textureX, textureY);
		bodyModel[106].addShapeBox(0, 0, 0, 4, 1, 19, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0);
		bodyModel[106].setRotationPoint(-35.0f, 3.25f, -9.5f);

		bodyModel[107] = new ModelRendererTurbo(this, 188, 94, textureX, textureY);
		bodyModel[107].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[107].setRotationPoint(-35.0f, 4.25f, 9.5f);

		bodyModel[108] = new ModelRendererTurbo(this, 185, 94, textureX, textureY);
		bodyModel[108].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[108].setRotationPoint(-35.0f, 4.25f, 10.5f);

		bodyModel[109] = new ModelRendererTurbo(this, 182, 94, textureX, textureY);
		bodyModel[109].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[109].setRotationPoint(-30.5f, 4.25f, 9.5f);

		bodyModel[110] = new ModelRendererTurbo(this, 179, 94, textureX, textureY);
		bodyModel[110].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[110].setRotationPoint(-30.5f, 4.25f, 10.5f);

		bodyModel[111] = new ModelRendererTurbo(this, 193, 78, textureX, textureY);
		bodyModel[111].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[111].setRotationPoint(-35.0f, 7.25f, 10.25f);

		bodyModel[112] = new ModelRendererTurbo(this, 108, 88, textureX, textureY);
		bodyModel[112].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[112].setRotationPoint(-35.0f, 6.25f, 10.25f);

		bodyModel[113] = new ModelRendererTurbo(this, 83, 78, textureX, textureY);
		bodyModel[113].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[113].setRotationPoint(-35.0f, 6.25f, 9.75f);

		bodyModel[114] = new ModelRendererTurbo(this, 75, 88, textureX, textureY);
		bodyModel[114].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[114].setRotationPoint(-35.0f, 5.25f, 10.0f);

		bodyModel[115] = new ModelRendererTurbo(this, 224, 53, textureX, textureY);
		bodyModel[115].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[115].setRotationPoint(-35.0f, 5.25f, 9.5f);

		bodyModel[116] = new ModelRendererTurbo(this, 38, 83, textureX, textureY);
		bodyModel[116].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[116].setRotationPoint(-35.0f, 4.25f, 9.75f);

		bodyModel[117] = new ModelRendererTurbo(this, 242, 38, textureX, textureY);
		bodyModel[117].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[117].setRotationPoint(-35.0f, 4.25f, 9.5f);

		bodyModel[118] = new ModelRendererTurbo(this, 165, 94, textureX, textureY);
		bodyModel[118].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[118].setRotationPoint(21.5f, 4.25f, -10.5f);

		bodyModel[119] = new ModelRendererTurbo(this, 117, 94, textureX, textureY);
		bodyModel[119].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0);
		bodyModel[119].setRotationPoint(21.5f, 4.25f, -11.5f);

		bodyModel[120] = new ModelRendererTurbo(this, 253, 89, textureX, textureY);
		bodyModel[120].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[120].setRotationPoint(26.0f, 4.25f, -10.5f);

		bodyModel[121] = new ModelRendererTurbo(this, 221, 84, textureX, textureY);
		bodyModel[121].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0);
		bodyModel[121].setRotationPoint(26.0f, 4.25f, -11.5f);

		bodyModel[122] = new ModelRendererTurbo(this, 233, 22, textureX, textureY);
		bodyModel[122].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[122].setRotationPoint(21.5f, 7.25f, -11.25f);

		bodyModel[123] = new ModelRendererTurbo(this, 34, 81, textureX, textureY);
		bodyModel[123].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[123].setRotationPoint(21.5f, 6.25f, -10.25f);

		bodyModel[124] = new ModelRendererTurbo(this, 221, 22, textureX, textureY);
		bodyModel[124].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[124].setRotationPoint(21.5f, 6.25f, -10.75f);

		bodyModel[125] = new ModelRendererTurbo(this, 135, 78, textureX, textureY);
		bodyModel[125].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[125].setRotationPoint(21.5f, 5.25f, -10.0f);

		bodyModel[126] = new ModelRendererTurbo(this, 209, 22, textureX, textureY);
		bodyModel[126].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[126].setRotationPoint(21.5f, 5.25f, -10.5f);

		bodyModel[127] = new ModelRendererTurbo(this, 135, 76, textureX, textureY);
		bodyModel[127].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[127].setRotationPoint(21.5f, 4.25f, -9.75f);

		bodyModel[128] = new ModelRendererTurbo(this, 197, 22, textureX, textureY);
		bodyModel[128].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[128].setRotationPoint(21.5f, 4.25f, -10.5f);

		bodyModel[129] = new ModelRendererTurbo(this, 207, 84, textureX, textureY);
		bodyModel[129].addShapeBox(0, 0, 0, 4, 1, 19, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0);
		bodyModel[129].setRotationPoint(21.5f, 3.25f, -9.5f);

		bodyModel[130] = new ModelRendererTurbo(this, 201, 84, textureX, textureY);
		bodyModel[130].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[130].setRotationPoint(21.5f, 4.25f, 9.5f);

		bodyModel[131] = new ModelRendererTurbo(this, 129, 77, textureX, textureY);
		bodyModel[131].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[131].setRotationPoint(21.5f, 4.25f, 10.5f);

		bodyModel[132] = new ModelRendererTurbo(this, 95, 77, textureX, textureY);
		bodyModel[132].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[132].setRotationPoint(26.0f, 4.25f, 9.5f);

		bodyModel[133] = new ModelRendererTurbo(this, 45, 74, textureX, textureY);
		bodyModel[133].addShapeBox(0, 0, 0, 0, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f);
		bodyModel[133].setRotationPoint(26.0f, 4.25f, 10.5f);

		bodyModel[134] = new ModelRendererTurbo(this, 185, 22, textureX, textureY);
		bodyModel[134].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[134].setRotationPoint(21.5f, 7.25f, 10.25f);

		bodyModel[135] = new ModelRendererTurbo(this, 199, 73, textureX, textureY);
		bodyModel[135].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[135].setRotationPoint(21.5f, 6.25f, 10.25f);

		bodyModel[136] = new ModelRendererTurbo(this, 173, 22, textureX, textureY);
		bodyModel[136].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[136].setRotationPoint(21.5f, 6.25f, 9.75f);

		bodyModel[137] = new ModelRendererTurbo(this, 244, 32, textureX, textureY);
		bodyModel[137].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[137].setRotationPoint(21.5f, 5.25f, 10.0f);

		bodyModel[138] = new ModelRendererTurbo(this, 161, 22, textureX, textureY);
		bodyModel[138].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[138].setRotationPoint(21.5f, 5.25f, 9.5f);

		bodyModel[139] = new ModelRendererTurbo(this, 245, 22, textureX, textureY);
		bodyModel[139].addShapeBox(0, 0, 0, 5, 1, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[139].setRotationPoint(21.5f, 4.25f, 9.75f);

		bodyModel[140] = new ModelRendererTurbo(this, 149, 22, textureX, textureY);
		bodyModel[140].addShapeBox(0, 0, 0, 5, 0, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[140].setRotationPoint(21.5f, 4.25f, 9.5f);

		bodyModel[141] = new ModelRendererTurbo(this, 182, 84, textureX, textureY);
		bodyModel[141].addShapeBox(0, 0, 0, 1, 20, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[141].setRotationPoint(-37.0f, -15.75f, -11.0f);

		bodyModel[142] = new ModelRendererTurbo(this, 0, 41, textureX, textureY);
		bodyModel[142].addShapeBox(0, 0, 0, 56, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, -0.375f, 0);
		bodyModel[142].setRotationPoint(-32.5f, 4.25f, -1.0f);

		bodyModel[143] = new ModelRendererTurbo(this, 108, 94, textureX, textureY);
		bodyModel[143].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 1, 0);
		bodyModel[143].setRotationPoint(-34.5f, 4.25f, -1.0f);

		bodyModel[144] = new ModelRendererTurbo(this, 25, 74, textureX, textureY);
		bodyModel[144].addShapeBox(0, 0, 0, 5, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[144].setRotationPoint(-39.5f, 4.25f, -1.0f);

		bodyModel[145] = new ModelRendererTurbo(this, 75, 94, textureX, textureY);
		bodyModel[145].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, 1, 0, 0, 1, 0, 0, -0.375f, 0);
		bodyModel[145].setRotationPoint(23.5f, 4.25f, -1.0f);

		bodyModel[146] = new ModelRendererTurbo(this, 117, 70, textureX, textureY);
		bodyModel[146].addShapeBox(0, 0, 0, 5, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[146].setRotationPoint(25.5f, 4.25f, -1.0f);

		bodyModel[147] = new ModelRendererTurbo(this, 0, 24, textureX, textureY);
		bodyModel[147].addShapeBox(0, 0, 0, 55, 1, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[147].setRotationPoint(-32.0f, -20.75f, -6.0f);

		bodyModel[148] = new ModelRendererTurbo(this, 0, 74, textureX, textureY);
		bodyModel[148].addShapeBox(0, 0, 0, 1, 20, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[148].setRotationPoint(27.0f, -15.75f, -11.0f);

		bodyModel[149] = new ModelRendererTurbo(this, 136, 148, textureX, textureY);
		bodyModel[149].addShapeBox(0, 0, 0, 3, 1, 12, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0);
		bodyModel[149].setRotationPoint(-35.0f, -20.75f, -6.0f);

		bodyModel[150] = new ModelRendererTurbo(this, 225, 160, textureX, textureY);
		bodyModel[150].addShapeBox(0, 0, 0, 1, 3, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, -1, 0, 0, -1, 0, 0, 1, 0, 0);
		bodyModel[150].setRotationPoint(-36.0f, -18.75f, -6.0f);

		bodyModel[151] = new ModelRendererTurbo(this, 72, 160, textureX, textureY);
		bodyModel[151].addShapeBox(0, 0, 0, 1, 1, 12, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[151].setRotationPoint(-36.0f, -19.75f, -6.0f);

		bodyModel[152] = new ModelRendererTurbo(this, 112, 156, textureX, textureY);
		bodyModel[152].addShapeBox(0, 0, 0, 1, 1, 12, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[152].setRotationPoint(26.0f, -19.75f, -6.0f);

		bodyModel[153] = new ModelRendererTurbo(this, 121, 38, textureX, textureY);
		bodyModel[153].addShapeBox(0, 0, 0, 59, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0);
		bodyModel[153].setRotationPoint(-34.0f, -19.75f, -5.0f);

		bodyModel[154] = new ModelRendererTurbo(this, 0, 38, textureX, textureY);
		bodyModel[154].addShapeBox(0, 0, 0, 59, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0);
		bodyModel[154].setRotationPoint(-34.0f, -19.75f, 4.0f);

		bodyModel[155] = new ModelRendererTurbo(this, 127, 17, textureX, textureY);
		bodyModel[155].addShapeBox(0, 0, 0, 62, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0);
		bodyModel[155].setRotationPoint(-35.5f, -18.75f, -5.0f);

		bodyModel[156] = new ModelRendererTurbo(this, 127, 12, textureX, textureY);
		bodyModel[156].addShapeBox(0, 0, 0, 62, 3, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0);
		bodyModel[156].setRotationPoint(-35.5f, -18.75f, 4.0f);

		bodyModel[157] = new ModelRendererTurbo(this, 127, 6, textureX, textureY);
		bodyModel[157].addShapeBox(0, 0, 0, 59, 1, 4, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0.25f, 0, 0, 0.25f, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[157].setRotationPoint(-34.0f, -17.5f, -8.75f);

		bodyModel[158] = new ModelRendererTurbo(this, 123, 28, textureX, textureY);
		bodyModel[158].addShapeBox(0, 0, 0, 59, 1, 2, 0, 0, -0.5f, -0.375f, 0, -0.5f, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f, 0, 0, 0, 0, 0, 0);
		bodyModel[158].setRotationPoint(-34.0f, -17.25f, -10.75f);

		bodyModel[159] = new ModelRendererTurbo(this, 135, 35, textureX, textureY);
		bodyModel[159].addShapeBox(0, 0, 0, 59, 1, 1, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0);
		bodyModel[159].setRotationPoint(-34.0f, -16.75f, -11.375f);

		bodyModel[160] = new ModelRendererTurbo(this, 127, 0, textureX, textureY);
		bodyModel[160].addShapeBox(0, 0, 0, 59, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0.25f, 0, 0, 0.25f, 0);
		bodyModel[160].setRotationPoint(-34.0f, -17.5f, 4.75f);

		bodyModel[161] = new ModelRendererTurbo(this, 123, 24, textureX, textureY);
		bodyModel[161].addShapeBox(0, 0, 0, 59, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, -0.375f, 0, -0.5f, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f);
		bodyModel[161].setRotationPoint(-34.0f, -17.25f, 8.75f);

		bodyModel[162] = new ModelRendererTurbo(this, 123, 32, textureX, textureY);
		bodyModel[162].addShapeBox(0, 0, 0, 59, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[162].setRotationPoint(-34.0f, -16.75f, 10.375f);

		bodyModel[163] = new ModelRendererTurbo(this, 36, 88, textureX, textureY);
		bodyModel[163].addShapeBox(0, 0, 0, 1, 1, 4, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[163].setRotationPoint(-37.0f, -16.75f, 4.75f);

		bodyModel[164] = new ModelRendererTurbo(this, 244, 93, textureX, textureY);
		bodyModel[164].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, -0.5f, 0, 0, 0, 0, 0, -0.5f, -0.375f, 0, -0.8125f, -0.375f, 0, 0.5f, 0, 0, 0, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f);
		bodyModel[164].setRotationPoint(-36.0f, -17.25f, 8.75f);

		bodyModel[165] = new ModelRendererTurbo(this, 158, 94, textureX, textureY);
		bodyModel[165].addShapeBox(0, 0, 0, 1, 1, 2, 0, -1, -0.5f, 0, 0, -0.5f, 0, 0, -0.8125f, -0.375f, -1, -0.8125f, -0.375f, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f);
		bodyModel[165].setRotationPoint(-37.0f, -17.25f, 8.75f);

		bodyModel[166] = new ModelRendererTurbo(this, 182, 89, textureX, textureY);
		bodyModel[166].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -0.3125f, 0, 0, 0, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[166].setRotationPoint(-36.0f, -16.75f, 10.375f);

		bodyModel[167] = new ModelRendererTurbo(this, 174, 76, textureX, textureY);
		bodyModel[167].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -1, 0, 0, -0.3125f, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[167].setRotationPoint(-37.0f, -16.75f, 10.375f);

		bodyModel[168] = new ModelRendererTurbo(this, 0, 45, textureX, textureY);
		bodyModel[168].addShapeBox(0, 0, 0, 52, 19, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0.125f, 0, 0, 0.125f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[168].setRotationPoint(-30.5f, -15.75f, -11.0f);

		bodyModel[169] = new ModelRendererTurbo(this, 83, 70, textureX, textureY);
		bodyModel[169].addShapeBox(0, 0, 0, 6, 3, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0.125f, 0, 0, 0.125f, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[169].setRotationPoint(-36.0f, -15.75f, -11.0f);

		bodyModel[170] = new ModelRendererTurbo(this, 117, 41, textureX, textureY);
		bodyModel[170].addShapeBox(0, 0, 0, 52, 19, 1, 0, 0, 0.125f, 0, 0, 0.125f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[170].setRotationPoint(-30.5f, -15.75f, 10.0f);

		bodyModel[171] = new ModelRendererTurbo(this, 241, 50, textureX, textureY);
		bodyModel[171].addShapeBox(0, 0, 0, 6, 3, 1, 0, 0, 0.125f, 0, -0.5f, 0.125f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[171].setRotationPoint(-36.0f, -15.75f, 10.0f);

		bodyModel[172] = new ModelRendererTurbo(this, 157, 70, textureX, textureY);
		bodyModel[172].addShapeBox(0, 0, 0, 1, 1, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[172].setRotationPoint(26.0f, 3.25f, -11.0f);

		bodyModel[173] = new ModelRendererTurbo(this, 110, 70, textureX, textureY);
		bodyModel[173].addShapeBox(0, 0, 0, 1, 1, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[173].setRotationPoint(-36.0f, 3.25f, -11.0f);

		bodyModel[174] = new ModelRendererTurbo(this, 139, 162, textureX, textureY);
		bodyModel[174].addShapeBox(0, 0, 0, 1, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[174].setRotationPoint(-36.0f, -12.75f, -11.0f);

		bodyModel[175] = new ModelRendererTurbo(this, 51, 142, textureX, textureY);
		bodyModel[175].addShapeBox(0, 0, 0, 1, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[175].setRotationPoint(-36.0f, -12.75f, 10.0f);

		bodyModel[176] = new ModelRendererTurbo(this, 248, 123, textureX, textureY);
		bodyModel[176].addShapeBox(0, 0, 0, 1, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[176].setRotationPoint(26.0f, -12.75f, -11.0f);

		bodyModel[177] = new ModelRendererTurbo(this, 159, 107, textureX, textureY);
		bodyModel[177].addShapeBox(0, 0, 0, 1, 16, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[177].setRotationPoint(26.0f, -12.75f, 10.0f);

		bodyModel[178] = new ModelRendererTurbo(this, 241, 45, textureX, textureY);
		bodyModel[178].addShapeBox(0, 0, 0, 6, 3, 1, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0.125f, 0, 0, 0.125f, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[178].setRotationPoint(21.5f, -15.75f, -11.0f);

		bodyModel[179] = new ModelRendererTurbo(this, 241, 40, textureX, textureY);
		bodyModel[179].addShapeBox(0, 0, 0, 6, 3, 1, 0, 0, 0.125f, 0, -0.5f, 0.125f, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0, 0, 0);
		bodyModel[179].setRotationPoint(21.5f, -15.75f, 10.0f);

		bodyModel[180] = new ModelRendererTurbo(this, 243, 105, textureX, textureY);
		bodyModel[180].addShapeBox(0, 0, 0, 4, 16, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0);
		bodyModel[180].setRotationPoint(-35.0f, -12.75f, -10.5f);

		bodyModel[181] = new ModelRendererTurbo(this, 232, 105, textureX, textureY);
		bodyModel[181].addShapeBox(0, 0, 0, 4, 16, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0);
		bodyModel[181].setRotationPoint(-35.0f, -12.75f, 9.5f);

		bodyModel[182] = new ModelRendererTurbo(this, 168, 94, textureX, textureY);
		bodyModel[182].addShapeBox(0, 0, 0, 4, 16, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0);
		bodyModel[182].setRotationPoint(21.5f, -12.75f, -10.5f);

		bodyModel[183] = new ModelRendererTurbo(this, 47, 88, textureX, textureY);
		bodyModel[183].addShapeBox(0, 0, 0, 4, 16, 1, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0, 0, 0);
		bodyModel[183].setRotationPoint(21.5f, -12.75f, 9.5f);

		bodyModel[184] = new ModelRendererTurbo(this, 223, 146, textureX, textureY);
		bodyModel[184].addShapeBox(0, 0, 0, 3, 1, 12, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0);
		bodyModel[184].setRotationPoint(23.0f, -20.75f, -6.0f);

		bodyModel[185] = new ModelRendererTurbo(this, 97, 152, textureX, textureY);
		bodyModel[185].addShapeBox(0, 0, 0, 1, 3, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 1, 0, 0, 1, 0, 0, -1, 0, 0);
		bodyModel[185].setRotationPoint(26.0f, -18.75f, -6.0f);

		bodyModel[186] = new ModelRendererTurbo(this, 135, 83, textureX, textureY);
		bodyModel[186].addShapeBox(0, 0, 0, 2, 1, 4, 0, 0, 0, 0, 0, 0.75f, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.75f, 0, 0, -0.5f, 0, 0, 0, 0);
		bodyModel[186].setRotationPoint(-36.0f, -16.75f, 4.75f);

		bodyModel[187] = new ModelRendererTurbo(this, 214, 86, textureX, textureY);
		bodyModel[187].addShapeBox(0, 0, 0, 1, 1, 4, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[187].setRotationPoint(-37.0f, -16.75f, -8.75f);

		bodyModel[188] = new ModelRendererTurbo(this, 235, 93, textureX, textureY);
		bodyModel[188].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, -0.8125f, -0.375f, 0, -0.5f, -0.375f, 0, 0, 0, 0, -0.5f, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f, 0, 0, 0, 0, 0.5f, 0);
		bodyModel[188].setRotationPoint(-36.0f, -17.25f, -10.75f);

		bodyModel[189] = new ModelRendererTurbo(this, 151, 94, textureX, textureY);
		bodyModel[189].addShapeBox(0, 0, 0, 1, 1, 2, 0, -1, -0.8125f, -0.375f, 0, -0.8125f, -0.375f, 0, -0.5f, 0, -1, -0.5f, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f, 0, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[189].setRotationPoint(-37.0f, -17.25f, -10.75f);

		bodyModel[190] = new ModelRendererTurbo(this, 157, 89, textureX, textureY);
		bodyModel[190].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, 0, 0, 0, -0.3125f, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0);
		bodyModel[190].setRotationPoint(-36.0f, -16.75f, -11.375f);

		bodyModel[191] = new ModelRendererTurbo(this, 174, 70, textureX, textureY);
		bodyModel[191].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, -0.3125f, 0, 0, -1, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0);
		bodyModel[191].setRotationPoint(-37.0f, -16.75f, -11.375f);

		bodyModel[192] = new ModelRendererTurbo(this, 25, 79, textureX, textureY);
		bodyModel[192].addShapeBox(0, 0, 0, 2, 1, 4, 0, 0, 0, 0, 0, 0.5f, 0, 0, 0.75f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.75f, 0, 0, 0, 0);
		bodyModel[192].setRotationPoint(-36.0f, -16.75f, -8.75f);

		bodyModel[193] = new ModelRendererTurbo(this, 166, 85, textureX, textureY);
		bodyModel[193].addShapeBox(0, 0, 0, 1, 1, 4, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[193].setRotationPoint(27.0f, -16.75f, 4.75f);

		bodyModel[194] = new ModelRendererTurbo(this, 216, 92, textureX, textureY);
		bodyModel[194].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, 0, 0, 0, -0.5f, 0, 0, -0.8125f, -0.375f, 0, -0.5f, -0.375f, 0, 0, 0, 0, 0.5f, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f);
		bodyModel[194].setRotationPoint(25.0f, -17.25f, 8.75f);

		bodyModel[195] = new ModelRendererTurbo(this, 82, 92, textureX, textureY);
		bodyModel[195].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, -0.5f, 0, -1, -0.5f, 0, -1, -0.8125f, -0.375f, 0, -0.8125f, -0.375f, 0, 0.5f, 0, 0, 0.5f, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f);
		bodyModel[195].setRotationPoint(27.0f, -17.25f, 8.75f);

		bodyModel[196] = new ModelRendererTurbo(this, 25, 85, textureX, textureY);
		bodyModel[196].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, 0, 0, 0, -0.3125f, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[196].setRotationPoint(25.0f, -16.75f, 10.375f);

		bodyModel[197] = new ModelRendererTurbo(this, 251, 66, textureX, textureY);
		bodyModel[197].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -0.3125f, 0, 0, -1, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, 0, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f);
		bodyModel[197].setRotationPoint(27.0f, -16.75f, 10.375f);

		bodyModel[198] = new ModelRendererTurbo(this, 117, 78, textureX, textureY);
		bodyModel[198].addShapeBox(0, 0, 0, 2, 1, 4, 0, 0, 0.75f, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, -0.75f, 0, 0, 0, 0, 0, 0, 0, 0, -0.5f, 0);
		bodyModel[198].setRotationPoint(25.0f, -16.75f, 4.75f);

		bodyModel[199] = new ModelRendererTurbo(this, 244, 24, textureX, textureY);
		bodyModel[199].addShapeBox(0, 0, 0, 1, 1, 4, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[199].setRotationPoint(27.0f, -16.75f, -8.75f);

		bodyModel[200] = new ModelRendererTurbo(this, 170, 81, textureX, textureY);
		bodyModel[200].addShapeBox(0, 0, 0, 2, 1, 2, 0, 0, -0.5f, -0.375f, 0, -0.8125f, -0.375f, 0, -0.5f, 0, 0, 0, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f, 0, 0.5f, 0, 0, 0, 0);
		bodyModel[200].setRotationPoint(25.0f, -17.25f, -10.75f);

		bodyModel[201] = new ModelRendererTurbo(this, 15, 79, textureX, textureY);
		bodyModel[201].addShapeBox(0, 0, 0, 1, 1, 2, 0, 0, -0.8125f, -0.375f, -1, -0.8125f, -0.375f, -1, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, -0.375f, 0, 0.5f, -0.375f, 0, 0.5f, 0, 0, 0.5f, 0);
		bodyModel[201].setRotationPoint(27.0f, -17.25f, -10.75f);

		bodyModel[202] = new ModelRendererTurbo(this, 14, 74, textureX, textureY);
		bodyModel[202].addShapeBox(0, 0, 0, 2, 1, 1, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, -0.3125f, 0, 0, 0, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0);
		bodyModel[202].setRotationPoint(25.0f, -16.75f, -11.375f);

		bodyModel[203] = new ModelRendererTurbo(this, 250, 58, textureX, textureY);
		bodyModel[203].addShapeBox(0, 0, 0, 1, 1, 1, 0, 0, -1, -0.375f, 0, -1, -0.375f, 0, -1, 0, 0, -0.3125f, 0, 0, 0, -0.375f, 0, 0, -0.375f, 0, 0, 0, 0, 0, 0);
		bodyModel[203].setRotationPoint(27.0f, -16.75f, -11.375f);

		bodyModel[204] = new ModelRendererTurbo(this, 36, 75, textureX, textureY);
		bodyModel[204].addShapeBox(0, 0, 0, 2, 1, 4, 0, 0, 0.5f, 0, 0, 0, 0, 0, 0, 0, 0, 0.75f, 0, 0, -0.5f, 0, 0, 0, 0, 0, 0, 0, 0, -0.75f, 0);
		bodyModel[204].setRotationPoint(25.0f, -16.75f, -8.75f);

		bodyModel[205] = new ModelRendererTurbo(this, 145, 107, textureX, textureY);
		bodyModel[205].addShapeBox(0, 0, 0, 1, 20, 20, 0, 0, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[205].setRotationPoint(19.5f, -16.5f, -10.0f);

		bodyModel[206] = new ModelRendererTurbo(this, 237, 76, textureX, textureY);
		bodyModel[206].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25f, 0, 0, 0.25f, 0, 0, 0.25f, 0, 0, 0.25f, 0);
		bodyModel[206].setRotationPoint(19.5f, -19.75f, -4.0f);

		bodyModel[207] = new ModelRendererTurbo(this, 102, 94, textureX, textureY);
		bodyModel[207].addShapeBox(0, 0, 0, 1, 20, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0, 0, -0.25f, 0);
		bodyModel[207].setRotationPoint(-29.5f, -16.5f, -10.0f);

		bodyModel[208] = new ModelRendererTurbo(this, 182, 76, textureX, textureY);
		bodyModel[208].addShapeBox(0, 0, 0, 1, 3, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.25f, 0, 0, 0.25f, 0, 0, 0.25f, 0, 0, 0.25f, 0);
		bodyModel[208].setRotationPoint(-29.5f, -19.75f, -4.0f);

		bodyModel[209] = new ModelRendererTurbo(this, 36, 109, textureX, textureY);
		bodyModel[209].addShapeBox(0, 0, 0, 7, 1, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[209].setRotationPoint(-22.0f, 2.25f, -8.0f);

		bodyModel[210] = new ModelRendererTurbo(this, 125, 94, textureX, textureY);
		bodyModel[210].addShapeBox(0, 0, 0, 7, 1, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[210].setRotationPoint(-8.0f, 2.25f, -8.0f);

		bodyModel[211] = new ModelRendererTurbo(this, 135, 70, textureX, textureY);
		bodyModel[211].addShapeBox(0, 0, 0, 7, 1, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[211].setRotationPoint(6.0f, 2.25f, -8.0f);

		bodyModel[212] = new ModelRendererTurbo(this, 83, 70, textureX, textureY);
		bodyModel[212].addShapeBox(0, 0, 0, 9, 2, 15, 0, -0.25f, 0.25f, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[212].setRotationPoint(-23.0f, 0.25f, -10.0f);

		bodyModel[213] = new ModelRendererTurbo(this, 51, 145, textureX, textureY);
		bodyModel[213].addShapeBox(0, 0, 0, 1, 7, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0);
		bodyModel[213].setRotationPoint(-13.5f, -6.75f, -10.0f);

		bodyModel[214] = new ModelRendererTurbo(this, 173, 144, textureX, textureY);
		bodyModel[214].addShapeBox(0, 0, 0, 1, 2, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0);
		bodyModel[214].setRotationPoint(-14.0f, 0.25f, -10.0f);

		bodyModel[215] = new ModelRendererTurbo(this, 34, 70, textureX, textureY);
		bodyModel[215].addShapeBox(0, 0, 0, 9, 2, 15, 0, -0.25f, 0.25f, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[215].setRotationPoint(-9.0f, 0.25f, -10.0f);

		bodyModel[216] = new ModelRendererTurbo(this, 18, 140, textureX, textureY);
		bodyModel[216].addShapeBox(0, 0, 0, 1, 7, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0);
		bodyModel[216].setRotationPoint(0.5f, -6.75f, -10.0f);

		bodyModel[217] = new ModelRendererTurbo(this, 115, 137, textureX, textureY);
		bodyModel[217].addShapeBox(0, 0, 0, 1, 2, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0);
		bodyModel[217].setRotationPoint(0.0f, 0.25f, -10.0f);

		bodyModel[218] = new ModelRendererTurbo(this, 196, 66, textureX, textureY);
		bodyModel[218].addShapeBox(0, 0, 0, 9, 2, 15, 0, -0.25f, 0.25f, 0, 0, 0, 0, 0, 0, 0, -0.25f, 0.25f, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[218].setRotationPoint(5.0f, 0.25f, -10.0f);

		bodyModel[219] = new ModelRendererTurbo(this, 76, 137, textureX, textureY);
		bodyModel[219].addShapeBox(0, 0, 0, 1, 7, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.5f, 0, 0, -0.5f, 0, 0, -0.5f, 0, 0, 0.5f, 0, 0);
		bodyModel[219].setRotationPoint(14.5f, -6.75f, -10.0f);

		bodyModel[220] = new ModelRendererTurbo(this, 58, 127, textureX, textureY);
		bodyModel[220].addShapeBox(0, 0, 0, 1, 2, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0);
		bodyModel[220].setRotationPoint(14.0f, 0.25f, -10.0f);

		bodyModel[221] = new ModelRendererTurbo(this, 0, 13, textureX, textureY);
		bodyModel[221].addShapeBox(0, 0, 0, 2, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[221].setRotationPoint(28.0f, 2.25f, -4.0f);

		bodyModel[222] = new ModelRendererTurbo(this, 206, 146, textureX, textureY);
		bodyModel[222].addShapeBox(0, 0, 0, 1, 2, 14, 0, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[222].setRotationPoint(28.0f, -14.75f, -7.0f);

		bodyModel[223] = new ModelRendererTurbo(this, 152, 107, textureX, textureY);
		bodyModel[223].addShapeBox(0, 0, 0, 1, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[223].setRotationPoint(28.0f, -12.75f, -7.0f);

		bodyModel[224] = new ModelRendererTurbo(this, 145, 107, textureX, textureY);
		bodyModel[224].addShapeBox(0, 0, 0, 1, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[224].setRotationPoint(28.0f, -12.75f, 5.0f);

		bodyModel[225] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[225].addShapeBox(0, 0, 0, 2, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[225].setRotationPoint(-39.0f, 2.25f, -4.0f);

		bodyModel[226] = new ModelRendererTurbo(this, 224, 41, textureX, textureY);
		bodyModel[226].addShapeBox(0, 0, 0, 1, 2, 14, 0, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[226].setRotationPoint(-38.0f, -14.75f, -7.0f);

		bodyModel[227] = new ModelRendererTurbo(this, 95, 105, textureX, textureY);
		bodyModel[227].addShapeBox(0, 0, 0, 1, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[227].setRotationPoint(-38.0f, -12.75f, -7.0f);

		bodyModel[228] = new ModelRendererTurbo(this, 58, 88, textureX, textureY);
		bodyModel[228].addShapeBox(0, 0, 0, 1, 16, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[228].setRotationPoint(-38.0f, -12.75f, 5.0f);

		bodyModel[229] = new ModelRendererTurbo(this, 0, 74, textureX, textureY);
		bodyModel[229].addShapeBox(0, 0, 0, 0, 2, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[229].setRotationPoint(29.0f, -12.75f, -5.0f);

		bodyModel[230] = new ModelRendererTurbo(this, 0, 0, textureX, textureY);
		bodyModel[230].addShapeBox(0, 0, 0, 0, 2, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		bodyModel[230].setRotationPoint(-38.0f, -12.75f, -5.0f);
		flipAll();
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		for(int i = 0; i < 231; i++) {
			bodyModel[i].render(f5);
		}

		if (ClientProxy.isHoliday()) {
			GL11.glPushMatrix();
			GL11.glTranslatef(-1.40125f, 0.0f, -0.72f);
			GL11.glRotatef(180f,0,0,1);
			lights.render(5);
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			GL11.glTranslatef(-0.28125f, 0.0f, -0.72f);
			GL11.glRotatef(180f,0,0,1);
			lights.render(5);
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			GL11.glTranslatef(0.83875f, 0.0f, -0.72f);
			GL11.glRotatef(180f,0,0,1);
			lights.render(5);
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			GL11.glTranslatef(-1.40125f, 0.0f, 0.72f);
			GL11.glRotatef(180f, 0, 1, 0);
			GL11.glRotatef(180f, 0, 0, 1);
			lights.render(5);
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			GL11.glTranslatef(-0.28125f, 0.0f, 0.72f);
			GL11.glRotatef(180f, 0, 1, 0);
			GL11.glRotatef(180f, 0, 0, 1);
			lights.render(5);
			GL11.glPopMatrix();

			GL11.glPushMatrix();
			GL11.glTranslatef(0.83875f, 0.0f, 0.72f);
			GL11.glRotatef(180f, 0, 1, 0);
			GL11.glRotatef(180f, 0, 0, 1);
			lights.render(5);
			GL11.glPopMatrix();
		}
	}
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {}
}
