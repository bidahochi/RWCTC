//This File was created with the Minecraft-SMP Modelling Toolbox 2.3.0.0
// Copyright (C) 2023 Minecraft-SMP.de
// This file is for Flan's Flying Mod Version 4.0.x+

// Model: 
// Model Creator: 
// Created on: 27.03.2023 - 14:37:15
// Last changed on: 27.03.2023 - 14:37:15

package train.client.render.models; //Path where the model is located


import net.minecraft.entity.Entity;
import org.lwjgl.opengl.GL11;
import tmt.ModelConverter;
import tmt.ModelRendererTurbo;
import train.common.api.Freight;

public class ModelBTRLogCar2 extends ModelConverter //Same as Filename
{
	int textureX = 1024;
	int textureY = 1024;

	public ModelBTRLogCar2() //Same as Filename
	{
		bodyModel = new ModelRendererTurbo[220];

		initbodyModel_1();

		translateAll(0F, 0F, 0F);


		flipAll();
	}

	private void initbodyModel_1()
	{
		bodyModel[0] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 3
		bodyModel[1] = new ModelRendererTurbo(this, 25, 1, textureX, textureY); // Box 4
		bodyModel[2] = new ModelRendererTurbo(this, 49, 1, textureX, textureY); // Box 5
		bodyModel[3] = new ModelRendererTurbo(this, 73, 1, textureX, textureY); // Box 6
		bodyModel[4] = new ModelRendererTurbo(this, 97, 1, textureX, textureY); // Box 7
		bodyModel[5] = new ModelRendererTurbo(this, 121, 1, textureX, textureY); // Box 8
		bodyModel[6] = new ModelRendererTurbo(this, 145, 1, textureX, textureY); // Box 9
		bodyModel[7] = new ModelRendererTurbo(this, 169, 1, textureX, textureY); // Box 10
		bodyModel[8] = new ModelRendererTurbo(this, 177, 1, textureX, textureY); // Box 11
		bodyModel[9] = new ModelRendererTurbo(this, 257, 1, textureX, textureY); // Box 12
		bodyModel[10] = new ModelRendererTurbo(this, 17, 1, textureX, textureY); // Box 19
		bodyModel[11] = new ModelRendererTurbo(this, 41, 1, textureX, textureY); // Box 20
		bodyModel[12] = new ModelRendererTurbo(this, 65, 1, textureX, textureY); // Box 21
		bodyModel[13] = new ModelRendererTurbo(this, 89, 1, textureX, textureY); // Box 22
		bodyModel[14] = new ModelRendererTurbo(this, 113, 1, textureX, textureY); // Box 23
		bodyModel[15] = new ModelRendererTurbo(this, 137, 1, textureX, textureY); // Box 24
		bodyModel[16] = new ModelRendererTurbo(this, 161, 1, textureX, textureY); // Box 25
		bodyModel[17] = new ModelRendererTurbo(this, 241, 1, textureX, textureY); // Box 26
		bodyModel[18] = new ModelRendererTurbo(this, 321, 1, textureX, textureY, "a1"); // log2
		bodyModel[19] = new ModelRendererTurbo(this, 417, 1, textureX, textureY, "a1"); // log2
		bodyModel[20] = new ModelRendererTurbo(this, 513, 1, textureX, textureY, "a1"); // log2
		bodyModel[21] = new ModelRendererTurbo(this, 609, 1, textureX, textureY, "a1"); // log2
		bodyModel[22] = new ModelRendererTurbo(this, 705, 1, textureX, textureY, "a1"); // log2
		bodyModel[23] = new ModelRendererTurbo(this, 801, 1, textureX, textureY, "a1"); // log2
		bodyModel[24] = new ModelRendererTurbo(this, 889, 1, textureX, textureY, "a1"); // log2
		bodyModel[25] = new ModelRendererTurbo(this, 321, 9, textureX, textureY, "a1"); // log2
		bodyModel[26] = new ModelRendererTurbo(this, 417, 9, textureX, textureY, "a1"); // log2
		bodyModel[27] = new ModelRendererTurbo(this, 513, 9, textureX, textureY, "a1"); // log2
		bodyModel[28] = new ModelRendererTurbo(this, 609, 9, textureX, textureY, "a1"); // log2
		bodyModel[29] = new ModelRendererTurbo(this, 705, 9, textureX, textureY, "a2"); // log
		bodyModel[30] = new ModelRendererTurbo(this, 1, 17, textureX, textureY, "a2"); // log
		bodyModel[31] = new ModelRendererTurbo(this, 337, 17, textureX, textureY, "a2"); // log
		bodyModel[32] = new ModelRendererTurbo(this, 793, 9, textureX, textureY, "a2"); // log
		bodyModel[33] = new ModelRendererTurbo(this, 433, 17, textureX, textureY, "a2"); // log
		bodyModel[34] = new ModelRendererTurbo(this, 529, 17, textureX, textureY, "a2"); // log
		bodyModel[35] = new ModelRendererTurbo(this, 625, 17, textureX, textureY, "a2"); // log
		bodyModel[36] = new ModelRendererTurbo(this, 897, 17, textureX, textureY, "a2"); // log
		bodyModel[37] = new ModelRendererTurbo(this, 1, 25, textureX, textureY, "a2"); // log
		bodyModel[38] = new ModelRendererTurbo(this, 97, 25, textureX, textureY, "a2"); // log
		bodyModel[39] = new ModelRendererTurbo(this, 193, 25, textureX, textureY, "a2"); // log
		bodyModel[40] = new ModelRendererTurbo(this, 289, 25, textureX, textureY, "a3"); // log3
		bodyModel[41] = new ModelRendererTurbo(this, 385, 25, textureX, textureY, "a3"); // log3
		bodyModel[42] = new ModelRendererTurbo(this, 481, 25, textureX, textureY, "a3"); // log3
		bodyModel[43] = new ModelRendererTurbo(this, 569, 25, textureX, textureY, "a3"); // log3
		bodyModel[44] = new ModelRendererTurbo(this, 665, 25, textureX, textureY, "a3"); // log3
		bodyModel[45] = new ModelRendererTurbo(this, 761, 25, textureX, textureY, "a3"); // log3
		bodyModel[46] = new ModelRendererTurbo(this, 857, 25, textureX, textureY, "a3"); // log3
		bodyModel[47] = new ModelRendererTurbo(this, 1, 33, textureX, textureY, "a3"); // log3
		bodyModel[48] = new ModelRendererTurbo(this, 97, 33, textureX, textureY, "a3"); // log3
		bodyModel[49] = new ModelRendererTurbo(this, 193, 33, textureX, textureY, "a3"); // log3
		bodyModel[50] = new ModelRendererTurbo(this, 289, 33, textureX, textureY, "a3"); // log3
		bodyModel[51] = new ModelRendererTurbo(this, 1, 1, textureX, textureY); // Box 77
		bodyModel[52] = new ModelRendererTurbo(this, 185, 1, textureX, textureY); // Box 78
		bodyModel[53] = new ModelRendererTurbo(this, 257, 1, textureX, textureY); // Box 79
		bodyModel[54] = new ModelRendererTurbo(this, 985, 1, textureX, textureY); // Box 80
		bodyModel[55] = new ModelRendererTurbo(this, 1001, 1, textureX, textureY); // Box 81
		bodyModel[56] = new ModelRendererTurbo(this, 241, 9, textureX, textureY); // Box 82
		bodyModel[57] = new ModelRendererTurbo(this, 385, 33, textureX, textureY, "a4"); // log4
		bodyModel[58] = new ModelRendererTurbo(this, 673, 33, textureX, textureY, "a4"); // log4
		bodyModel[59] = new ModelRendererTurbo(this, 769, 33, textureX, textureY, "a4"); // log4
		bodyModel[60] = new ModelRendererTurbo(this, 865, 33, textureX, textureY, "a4"); // log4
		bodyModel[61] = new ModelRendererTurbo(this, 473, 33, textureX, textureY, "a4"); // log4
		bodyModel[62] = new ModelRendererTurbo(this, 1, 41, textureX, textureY, "a4"); // log4
		bodyModel[63] = new ModelRendererTurbo(this, 97, 41, textureX, textureY, "a4"); // log4
		bodyModel[64] = new ModelRendererTurbo(this, 193, 41, textureX, textureY, "a4"); // log4
		bodyModel[65] = new ModelRendererTurbo(this, 289, 41, textureX, textureY, "a4"); // log4
		bodyModel[66] = new ModelRendererTurbo(this, 577, 41, textureX, textureY, "a4"); // log4
		bodyModel[67] = new ModelRendererTurbo(this, 673, 41, textureX, textureY, "a4"); // log4
		bodyModel[68] = new ModelRendererTurbo(this, 769, 41, textureX, textureY, "a5"); // log5
		bodyModel[69] = new ModelRendererTurbo(this, 865, 41, textureX, textureY, "a5"); // log5
		bodyModel[70] = new ModelRendererTurbo(this, 377, 41, textureX, textureY, "a5"); // Box 96 log
		bodyModel[71] = new ModelRendererTurbo(this, 1, 49, textureX, textureY, "a5"); // log5
		bodyModel[72] = new ModelRendererTurbo(this, 97, 49, textureX, textureY, "a5"); // log5
		bodyModel[73] = new ModelRendererTurbo(this, 193, 49, textureX, textureY, "a5"); // log5
		bodyModel[74] = new ModelRendererTurbo(this, 481, 49, textureX, textureY, "a5"); // log5
		bodyModel[75] = new ModelRendererTurbo(this, 577, 49, textureX, textureY, "a5"); // log5
		bodyModel[76] = new ModelRendererTurbo(this, 673, 49, textureX, textureY, "a5"); // log5
		bodyModel[77] = new ModelRendererTurbo(this, 769, 49, textureX, textureY, "a5"); // log5
		bodyModel[78] = new ModelRendererTurbo(this, 865, 49, textureX, textureY, "a5"); // log5
		bodyModel[79] = new ModelRendererTurbo(this, 1, 57, textureX, textureY, "a6"); // log6
		bodyModel[80] = new ModelRendererTurbo(this, 97, 57, textureX, textureY, "a6"); // log6
		bodyModel[81] = new ModelRendererTurbo(this, 193, 57, textureX, textureY, "a6"); // log6
		bodyModel[82] = new ModelRendererTurbo(this, 289, 57, textureX, textureY, "a6"); // log6
		bodyModel[83] = new ModelRendererTurbo(this, 385, 57, textureX, textureY, "a6"); // log6
		bodyModel[84] = new ModelRendererTurbo(this, 473, 57, textureX, textureY, "a6"); // log6
		bodyModel[85] = new ModelRendererTurbo(this, 569, 57, textureX, textureY, "a6"); // Box 111 log
		bodyModel[86] = new ModelRendererTurbo(this, 665, 57, textureX, textureY, "a6"); // log6
		bodyModel[87] = new ModelRendererTurbo(this, 761, 57, textureX, textureY, "a6"); // log6
		bodyModel[88] = new ModelRendererTurbo(this, 857, 57, textureX, textureY, "a6"); // log6
		bodyModel[89] = new ModelRendererTurbo(this, 1, 65, textureX, textureY, "a6"); // log6
		bodyModel[90] = new ModelRendererTurbo(this, 977, 9, textureX, textureY); // Box 116
		bodyModel[91] = new ModelRendererTurbo(this, 961, 33, textureX, textureY); // Box 117
		bodyModel[92] = new ModelRendererTurbo(this, 257, 9, textureX, textureY); // Box 118
		bodyModel[93] = new ModelRendererTurbo(this, 1001, 9, textureX, textureY); // Box 154
		bodyModel[94] = new ModelRendererTurbo(this, 97, 17, textureX, textureY); // Box 155
		bodyModel[95] = new ModelRendererTurbo(this, 113, 17, textureX, textureY); // Box 156
		bodyModel[96] = new ModelRendererTurbo(this, 129, 17, textureX, textureY); // Box 157
		bodyModel[97] = new ModelRendererTurbo(this, 1017, 1, textureX, textureY); // Box 160
		bodyModel[98] = new ModelRendererTurbo(this, 1017, 9, textureX, textureY); // Box 161
		bodyModel[99] = new ModelRendererTurbo(this, 145, 17, textureX, textureY); // Box 162
		bodyModel[100] = new ModelRendererTurbo(this, 153, 17, textureX, textureY); // Box 163
		bodyModel[101] = new ModelRendererTurbo(this, 937, 57, textureX, textureY); // Box 164
		bodyModel[102] = new ModelRendererTurbo(this, 985, 33, textureX, textureY); // Box 168
		bodyModel[103] = new ModelRendererTurbo(this, 1001, 41, textureX, textureY); // Box 168
		bodyModel[104] = new ModelRendererTurbo(this, 161, 17, textureX, textureY); // Box 161
		bodyModel[105] = new ModelRendererTurbo(this, 169, 17, textureX, textureY); // Box 161
		bodyModel[106] = new ModelRendererTurbo(this, 721, 17, textureX, textureY); // Box 161
		bodyModel[107] = new ModelRendererTurbo(this, 729, 17, textureX, textureY); // Box 161
		bodyModel[108] = new ModelRendererTurbo(this, 737, 17, textureX, textureY); // Box 161
		bodyModel[109] = new ModelRendererTurbo(this, 745, 17, textureX, textureY); // Box 161
		bodyModel[110] = new ModelRendererTurbo(this, 753, 17, textureX, textureY); // Box 161
		bodyModel[111] = new ModelRendererTurbo(this, 761, 17, textureX, textureY); // Box 161
		bodyModel[112] = new ModelRendererTurbo(this, 89, 65, textureX, textureY); // Box 138
		bodyModel[113] = new ModelRendererTurbo(this, 137, 65, textureX, textureY); // Box 143
		bodyModel[114] = new ModelRendererTurbo(this, 257, 65, textureX, textureY); // Box 144
		bodyModel[115] = new ModelRendererTurbo(this, 769, 17, textureX, textureY); // Box 156
		bodyModel[116] = new ModelRendererTurbo(this, 1001, 17, textureX, textureY); // Box 157
		bodyModel[117] = new ModelRendererTurbo(this, 785, 17, textureX, textureY); // Box 160
		bodyModel[118] = new ModelRendererTurbo(this, 1017, 17, textureX, textureY); // Box 161
		bodyModel[119] = new ModelRendererTurbo(this, 953, 25, textureX, textureY); // Box 162
		bodyModel[120] = new ModelRendererTurbo(this, 961, 25, textureX, textureY); // Box 163
		bodyModel[121] = new ModelRendererTurbo(this, 345, 65, textureX, textureY); // Box 164
		bodyModel[122] = new ModelRendererTurbo(this, 1001, 57, textureX, textureY); // Box 168
		bodyModel[123] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 168
		bodyModel[124] = new ModelRendererTurbo(this, 969, 25, textureX, textureY); // Box 161
		bodyModel[125] = new ModelRendererTurbo(this, 1017, 25, textureX, textureY); // Box 161
		bodyModel[126] = new ModelRendererTurbo(this, 961, 33, textureX, textureY); // Box 161
		bodyModel[127] = new ModelRendererTurbo(this, 969, 33, textureX, textureY); // Box 161
		bodyModel[128] = new ModelRendererTurbo(this, 985, 33, textureX, textureY); // Box 161
		bodyModel[129] = new ModelRendererTurbo(this, 1001, 33, textureX, textureY); // Box 161
		bodyModel[130] = new ModelRendererTurbo(this, 1009, 33, textureX, textureY); // Box 161
		bodyModel[131] = new ModelRendererTurbo(this, 1017, 33, textureX, textureY); // Box 161
		bodyModel[132] = new ModelRendererTurbo(this, 425, 65, textureX, textureY); // Box 138
		bodyModel[133] = new ModelRendererTurbo(this, 961, 41, textureX, textureY); // Box 163
		bodyModel[134] = new ModelRendererTurbo(this, 289, 49, textureX, textureY); // Box 164
		bodyModel[135] = new ModelRendererTurbo(this, 257, 9, textureX, textureY); // Box 135
		bodyModel[136] = new ModelRendererTurbo(this, 1017, 41, textureX, textureY); // Box 83
		bodyModel[137] = new ModelRendererTurbo(this, 321, 49, textureX, textureY); // Box 109
		bodyModel[138] = new ModelRendererTurbo(this, 329, 49, textureX, textureY); // Box 110
		bodyModel[139] = new ModelRendererTurbo(this, 337, 49, textureX, textureY); // Box 111
		bodyModel[140] = new ModelRendererTurbo(this, 345, 49, textureX, textureY); // Box 112
		bodyModel[141] = new ModelRendererTurbo(this, 353, 49, textureX, textureY); // Box 113
		bodyModel[142] = new ModelRendererTurbo(this, 361, 49, textureX, textureY); // Box 83
		bodyModel[143] = new ModelRendererTurbo(this, 369, 49, textureX, textureY); // Box 109
		bodyModel[144] = new ModelRendererTurbo(this, 985, 57, textureX, textureY); // Box 110
		bodyModel[145] = new ModelRendererTurbo(this, 993, 57, textureX, textureY); // Box 111
		bodyModel[146] = new ModelRendererTurbo(this, 1001, 57, textureX, textureY); // Box 112
		bodyModel[147] = new ModelRendererTurbo(this, 1017, 57, textureX, textureY); // Box 113
		bodyModel[148] = new ModelRendererTurbo(this, 393, 65, textureX, textureY); // Box 515
		bodyModel[149] = new ModelRendererTurbo(this, 401, 65, textureX, textureY); // Box 517
		bodyModel[150] = new ModelRendererTurbo(this, 409, 65, textureX, textureY); // Box 515
		bodyModel[151] = new ModelRendererTurbo(this, 425, 65, textureX, textureY); // Box 517
		bodyModel[152] = new ModelRendererTurbo(this, 577, 65, textureX, textureY); // Box 520
		bodyModel[153] = new ModelRendererTurbo(this, 585, 65, textureX, textureY); // Box 520
		bodyModel[154] = new ModelRendererTurbo(this, 593, 65, textureX, textureY); // Box 515
		bodyModel[155] = new ModelRendererTurbo(this, 601, 65, textureX, textureY); // Box 517
		bodyModel[156] = new ModelRendererTurbo(this, 609, 65, textureX, textureY); // Box 515
		bodyModel[157] = new ModelRendererTurbo(this, 617, 65, textureX, textureY); // Box 517
		bodyModel[158] = new ModelRendererTurbo(this, 625, 65, textureX, textureY); // Box 520
		bodyModel[159] = new ModelRendererTurbo(this, 633, 65, textureX, textureY); // Box 520
		bodyModel[160] = new ModelRendererTurbo(this, 641, 65, textureX, textureY); // Box 1
		bodyModel[161] = new ModelRendererTurbo(this, 657, 65, textureX, textureY); // Box 6
		bodyModel[162] = new ModelRendererTurbo(this, 673, 65, textureX, textureY); // Box 9
		bodyModel[163] = new ModelRendererTurbo(this, 745, 57, textureX, textureY); // Box 553
		bodyModel[164] = new ModelRendererTurbo(this, 689, 65, textureX, textureY); // Box 341
		bodyModel[165] = new ModelRendererTurbo(this, 697, 65, textureX, textureY); // Box 341
		bodyModel[166] = new ModelRendererTurbo(this, 705, 65, textureX, textureY); // Box 553
		bodyModel[167] = new ModelRendererTurbo(this, 729, 65, textureX, textureY); // Box 338
		bodyModel[168] = new ModelRendererTurbo(this, 737, 65, textureX, textureY); // Box 338
		bodyModel[169] = new ModelRendererTurbo(this, 777, 65, textureX, textureY); // Box 341
		bodyModel[170] = new ModelRendererTurbo(this, 785, 65, textureX, textureY); // Box 341
		bodyModel[171] = new ModelRendererTurbo(this, 793, 65, textureX, textureY); // Box 553
		bodyModel[172] = new ModelRendererTurbo(this, 817, 65, textureX, textureY); // Box 341
		bodyModel[173] = new ModelRendererTurbo(this, 825, 65, textureX, textureY); // Box 341
		bodyModel[174] = new ModelRendererTurbo(this, 833, 65, textureX, textureY); // Box 553
		bodyModel[175] = new ModelRendererTurbo(this, 745, 65, textureX, textureY); // Box 338
		bodyModel[176] = new ModelRendererTurbo(this, 761, 65, textureX, textureY); // Box 338
		bodyModel[177] = new ModelRendererTurbo(this, 857, 65, textureX, textureY); // Box 341
		bodyModel[178] = new ModelRendererTurbo(this, 865, 65, textureX, textureY); // Box 341
		bodyModel[179] = new ModelRendererTurbo(this, 873, 65, textureX, textureY); // Box 553
		bodyModel[180] = new ModelRendererTurbo(this, 561, 65, textureX, textureY); // Box 553
		bodyModel[181] = new ModelRendererTurbo(this, 897, 65, textureX, textureY); // Box 341
		bodyModel[182] = new ModelRendererTurbo(this, 1, 73, textureX, textureY); // Box 6
		bodyModel[183] = new ModelRendererTurbo(this, 17, 73, textureX, textureY); // Box 1
		bodyModel[184] = new ModelRendererTurbo(this, 33, 73, textureX, textureY); // Box 6
		bodyModel[185] = new ModelRendererTurbo(this, 49, 73, textureX, textureY); // Box 9
		bodyModel[186] = new ModelRendererTurbo(this, 65, 73, textureX, textureY); // Box 6
		bodyModel[187] = new ModelRendererTurbo(this, 145, 73, textureX, textureY); // Box 341
		bodyModel[188] = new ModelRendererTurbo(this, 769, 65, textureX, textureY); // Box 338
		bodyModel[189] = new ModelRendererTurbo(this, 81, 73, textureX, textureY); // Box 338
		bodyModel[190] = new ModelRendererTurbo(this, 185, 73, textureX, textureY); // Box 1
		bodyModel[191] = new ModelRendererTurbo(this, 201, 73, textureX, textureY); // Box 6
		bodyModel[192] = new ModelRendererTurbo(this, 217, 73, textureX, textureY); // Box 9
		bodyModel[193] = new ModelRendererTurbo(this, 641, 65, textureX, textureY); // Box 553
		bodyModel[194] = new ModelRendererTurbo(this, 233, 73, textureX, textureY); // Box 341
		bodyModel[195] = new ModelRendererTurbo(this, 241, 73, textureX, textureY); // Box 341
		bodyModel[196] = new ModelRendererTurbo(this, 249, 73, textureX, textureY); // Box 553
		bodyModel[197] = new ModelRendererTurbo(this, 273, 73, textureX, textureY); // Box 338
		bodyModel[198] = new ModelRendererTurbo(this, 281, 73, textureX, textureY); // Box 338
		bodyModel[199] = new ModelRendererTurbo(this, 289, 73, textureX, textureY); // Box 341
		bodyModel[200] = new ModelRendererTurbo(this, 297, 73, textureX, textureY); // Box 341
		bodyModel[201] = new ModelRendererTurbo(this, 305, 73, textureX, textureY); // Box 553
		bodyModel[202] = new ModelRendererTurbo(this, 329, 73, textureX, textureY); // Box 341
		bodyModel[203] = new ModelRendererTurbo(this, 337, 73, textureX, textureY); // Box 341
		bodyModel[204] = new ModelRendererTurbo(this, 473, 73, textureX, textureY); // Box 553
		bodyModel[205] = new ModelRendererTurbo(this, 497, 73, textureX, textureY); // Box 338
		bodyModel[206] = new ModelRendererTurbo(this, 505, 73, textureX, textureY); // Box 338
		bodyModel[207] = new ModelRendererTurbo(this, 513, 73, textureX, textureY); // Box 341
		bodyModel[208] = new ModelRendererTurbo(this, 521, 73, textureX, textureY); // Box 341
		bodyModel[209] = new ModelRendererTurbo(this, 529, 73, textureX, textureY); // Box 553
		bodyModel[210] = new ModelRendererTurbo(this, 673, 65, textureX, textureY); // Box 553
		bodyModel[211] = new ModelRendererTurbo(this, 593, 73, textureX, textureY); // Box 341
		bodyModel[212] = new ModelRendererTurbo(this, 553, 73, textureX, textureY); // Box 6
		bodyModel[213] = new ModelRendererTurbo(this, 577, 73, textureX, textureY); // Box 1
		bodyModel[214] = new ModelRendererTurbo(this, 633, 73, textureX, textureY); // Box 6
		bodyModel[215] = new ModelRendererTurbo(this, 657, 73, textureX, textureY); // Box 9
		bodyModel[216] = new ModelRendererTurbo(this, 673, 73, textureX, textureY); // Box 6
		bodyModel[217] = new ModelRendererTurbo(this, 705, 73, textureX, textureY); // Box 341
		bodyModel[218] = new ModelRendererTurbo(this, 689, 73, textureX, textureY); // Box 338
		bodyModel[219] = new ModelRendererTurbo(this, 697, 73, textureX, textureY); // Box 338

		bodyModel[0].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 3
		bodyModel[0].setRotationPoint(18F, 0.5F, -10F);

		bodyModel[1].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 4
		bodyModel[1].setRotationPoint(-22F, 0.5F, -10F);

		bodyModel[2].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 5
		bodyModel[2].setRotationPoint(-6F, 0.5F, -10F);

		bodyModel[3].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F); // Box 6
		bodyModel[3].setRotationPoint(4F, 0.5F, -10F);

		bodyModel[4].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 7
		bodyModel[4].setRotationPoint(-6F, 0.5F, 1F);

		bodyModel[5].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 8
		bodyModel[5].setRotationPoint(-22F, 0.5F, 1F);

		bodyModel[6].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		bodyModel[6].setRotationPoint(4F, 0.5F, 1F);

		bodyModel[7].addShapeBox(0F, 0F, 0F, 3, 1, 7, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 10
		bodyModel[7].setRotationPoint(18F, 0.5F, 1F);

		bodyModel[8].addShapeBox(0F, 0F, 0F, 21, 1, 18, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 11
		bodyModel[8].setRotationPoint(27F, 0.5F, -10F);

		bodyModel[9].addShapeBox(0F, 0F, 0F, 21, 1, 18, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 12
		bodyModel[9].setRotationPoint(-48F, 0.5F, -10F);

		bodyModel[10].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 19
		bodyModel[10].setRotationPoint(18F, -1.5F, 6F);

		bodyModel[11].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 20
		bodyModel[11].setRotationPoint(18F, -1.5F, -10F);

		bodyModel[12].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 21
		bodyModel[12].setRotationPoint(4F, -1.5F, 6F);

		bodyModel[13].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 22
		bodyModel[13].setRotationPoint(4F, -1.5F, -10F);

		bodyModel[14].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 23
		bodyModel[14].setRotationPoint(-6F, -1.5F, 6F);

		bodyModel[15].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 24
		bodyModel[15].setRotationPoint(-6F, -1.5F, -10F);

		bodyModel[16].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 25
		bodyModel[16].setRotationPoint(-22F, -1.5F, 6F);

		bodyModel[17].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 26
		bodyModel[17].setRotationPoint(-22F, -1.5F, -10F);

		bodyModel[18].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log2
		bodyModel[18].setRotationPoint(-43.5F, -2.5F, -8.5F);

		bodyModel[19].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // log2
		bodyModel[19].setRotationPoint(-43.5F, -2.5F, -4.5F);

		bodyModel[20].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log2
		bodyModel[20].setRotationPoint(-43.5F, -2.5F, -6.5F);

		bodyModel[21].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // log2
		bodyModel[21].setRotationPoint(-43.5F, -8.5F, -4.5F);

		bodyModel[22].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log2
		bodyModel[22].setRotationPoint(-43.5F, -8.5F, -6.5F);

		bodyModel[23].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log2
		bodyModel[23].setRotationPoint(-43.5F, -8.5F, -8.5F);

		bodyModel[24].addShapeBox(0F, 0F, 0F, 43, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // log2
		bodyModel[24].setRotationPoint(-43.5F, -5.5F, -9F);

		bodyModel[25].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // log2
		bodyModel[25].setRotationPoint(-43.5F, -4F, -2F);

		bodyModel[26].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log2
		bodyModel[26].setRotationPoint(-43.5F, -7F, -2F);

		bodyModel[27].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log2
		bodyModel[27].setRotationPoint(-43.5F, -4F, -10F);

		bodyModel[28].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log2
		bodyModel[28].setRotationPoint(-43.5F, -7F, -10F);

		bodyModel[29].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // log
		bodyModel[29].setRotationPoint(-43.5F, -2.5F, 4.5F);

		bodyModel[30].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // log
		bodyModel[30].setRotationPoint(-43.5F, -4F, 7F);

		bodyModel[31].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log
		bodyModel[31].setRotationPoint(-43.5F, -7F, 7F);

		bodyModel[32].addShapeBox(0F, 0F, 0F, 43, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // log
		bodyModel[32].setRotationPoint(-43.5F, -5.5F, 0F);

		bodyModel[33].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // log
		bodyModel[33].setRotationPoint(-43.5F, -8.5F, 4.5F);

		bodyModel[34].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log
		bodyModel[34].setRotationPoint(-43.5F, -8.5F, 2.5F);

		bodyModel[35].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log
		bodyModel[35].setRotationPoint(-43.5F, -8.5F, 0.5F);

		bodyModel[36].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log
		bodyModel[36].setRotationPoint(-43.5F, -7F, -1F);

		bodyModel[37].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log
		bodyModel[37].setRotationPoint(-43.5F, -4F, -1F);

		bodyModel[38].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log
		bodyModel[38].setRotationPoint(-43.5F, -2.5F, 0.5F);

		bodyModel[39].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log
		bodyModel[39].setRotationPoint(-43.5F, -2.5F, 2.5F);

		bodyModel[40].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // log3
		bodyModel[40].setRotationPoint(-43.5F, -11.5F, 2.5F);

		bodyModel[41].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log3
		bodyModel[41].setRotationPoint(-43.5F, -14.5F, 2.5F);

		bodyModel[42].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // log3
		bodyModel[42].setRotationPoint(-43.5F, -16F, 0F);

		bodyModel[43].addShapeBox(0F, 0F, 0F, 43, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // log3
		bodyModel[43].setRotationPoint(-43.5F, -13F, -4.5F);

		bodyModel[44].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log3
		bodyModel[44].setRotationPoint(-43.5F, -16F, -2F);

		bodyModel[45].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log3
		bodyModel[45].setRotationPoint(-43.5F, -10F, -2F);

		bodyModel[46].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // log3
		bodyModel[46].setRotationPoint(-43.5F, -10F, 0F);

		bodyModel[47].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log3
		bodyModel[47].setRotationPoint(-43.5F, -10F, -4F);

		bodyModel[48].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log3
		bodyModel[48].setRotationPoint(-43.5F, -16F, -4F);

		bodyModel[49].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log3
		bodyModel[49].setRotationPoint(-43.5F, -14.5F, -5.5F);

		bodyModel[50].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log3
		bodyModel[50].setRotationPoint(-43.5F, -11.5F, -5.5F);

		bodyModel[51].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 77
		bodyModel[51].setRotationPoint(-37.5F, 1F, -1F);

		bodyModel[52].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 78
		bodyModel[52].setRotationPoint(36.5F, 1F, -1.5F);

		bodyModel[53].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 79
		bodyModel[53].setRotationPoint(35.5F, -1.5F, -10F);

		bodyModel[54].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 80
		bodyModel[54].setRotationPoint(35.5F, -1.5F, 6F);

		bodyModel[55].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 81
		bodyModel[55].setRotationPoint(-38.5F, -1.5F, -10F);

		bodyModel[56].addShapeBox(0F, 0F, 0F, 3, 2, 2, 0F,0F, 0F, -2F, 0F, 0F, -2F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 82
		bodyModel[56].setRotationPoint(-38.5F, -1.5F, 6F);

		bodyModel[57].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log4
		bodyModel[57].setRotationPoint(0.5F, -4F, -10F);

		bodyModel[58].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log4
		bodyModel[58].setRotationPoint(0.5F, -2.5F, -8.5F);

		bodyModel[59].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log4
		bodyModel[59].setRotationPoint(0.5F, -7F, -10F);

		bodyModel[60].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log4
		bodyModel[60].setRotationPoint(0.5F, -8.5F, -8.5F);

		bodyModel[61].addShapeBox(0F, 0F, 0F, 43, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // log4
		bodyModel[61].setRotationPoint(0.5F, -5.5F, -9F);

		bodyModel[62].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log4
		bodyModel[62].setRotationPoint(0.5F, -8.5F, -6.5F);

		bodyModel[63].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // log4
		bodyModel[63].setRotationPoint(0.5F, -8.5F, -4.5F);

		bodyModel[64].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log4
		bodyModel[64].setRotationPoint(0.5F, -7F, -2F);

		bodyModel[65].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // log4
		bodyModel[65].setRotationPoint(0.5F, -4F, -2F);

		bodyModel[66].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // log4
		bodyModel[66].setRotationPoint(0.5F, -2.5F, -4.5F);

		bodyModel[67].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log4
		bodyModel[67].setRotationPoint(0.5F, -2.5F, -6.5F);

		bodyModel[68].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log5
		bodyModel[68].setRotationPoint(0.5F, -4F, -1F);

		bodyModel[69].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log5
		bodyModel[69].setRotationPoint(0.5F, -7F, -1F);

		bodyModel[70].addShapeBox(0F, 0F, 0F, 43, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // Box 96 log
		bodyModel[70].setRotationPoint(0.5F, -5.5F, 0F);

		bodyModel[71].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log5
		bodyModel[71].setRotationPoint(0.5F, -8.5F, 2.5F);

		bodyModel[72].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log5
		bodyModel[72].setRotationPoint(0.5F, -8.5F, 0.5F);

		bodyModel[73].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // log5
		bodyModel[73].setRotationPoint(0.5F, -8.5F, 4.5F);

		bodyModel[74].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log5
		bodyModel[74].setRotationPoint(0.5F, -7F, 7F);

		bodyModel[75].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // log5
		bodyModel[75].setRotationPoint(0.5F, -2.5F, 4.5F);

		bodyModel[76].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log5
		bodyModel[76].setRotationPoint(0.5F, -2.5F, 2.5F);

		bodyModel[77].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log5
		bodyModel[77].setRotationPoint(0.5F, -2.5F, 0.5F);

		bodyModel[78].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // log5
		bodyModel[78].setRotationPoint(0.5F, -4F, 7F);

		bodyModel[79].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F); // log6
		bodyModel[79].setRotationPoint(0.5F, -10F, 0F);

		bodyModel[80].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log6
		bodyModel[80].setRotationPoint(0.5F, -10F, -2F);

		bodyModel[81].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // log6
		bodyModel[81].setRotationPoint(0.5F, -10F, -4F);

		bodyModel[82].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log6
		bodyModel[82].setRotationPoint(0.5F, -11.5F, -5.5F);

		bodyModel[83].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log6
		bodyModel[83].setRotationPoint(0.5F, -14.5F, -5.5F);

		bodyModel[84].addShapeBox(0F, 0F, 0F, 43, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 1F); // log6
		bodyModel[84].setRotationPoint(0.5F, -13F, -4.5F);

		bodyModel[85].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 111 log
		bodyModel[85].setRotationPoint(0.5F, -16F, -4F);

		bodyModel[86].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log6
		bodyModel[86].setRotationPoint(0.5F, -16F, -2F);

		bodyModel[87].addShapeBox(0F, 0F, 0F, 43, 3, 2, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0.5F, 0F, -1.5F, 0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0.5F, 0F, 0F, 0.5F); // log6
		bodyModel[87].setRotationPoint(0.5F, -16F, 0F);

		bodyModel[88].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // log6
		bodyModel[88].setRotationPoint(0.5F, -14.5F, 2.5F);

		bodyModel[89].addShapeBox(0F, 0F, 0F, 43, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -1.5F, 0F, 0F, -1.5F, 0F); // log6
		bodyModel[89].setRotationPoint(0.5F, -11.5F, 2.5F);

		bodyModel[90].addShapeBox(0F, 0F, 0F, 1, 4, 18, 0F,-0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F); // Box 116
		bodyModel[90].setRotationPoint(-49F, 0.5F, -10F);

		bodyModel[91].addShapeBox(0F, 0F, 0F, 1, 4, 18, 0F,-0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F, -0.75F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.75F, 0F, 0F); // Box 117
		bodyModel[91].setRotationPoint(47.25F, 0.5F, -10F);

		bodyModel[92].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Box 118
		bodyModel[92].setRotationPoint(-51.2F, 1F, -2F);

		bodyModel[93].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 154
		bodyModel[93].setRotationPoint(38.5F, 3.5F, 4F);

		bodyModel[94].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 155
		bodyModel[94].setRotationPoint(29.5F, 3.5F, 4F);

		bodyModel[95].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 156
		bodyModel[95].setRotationPoint(29.5F, 3.5F, -6F);

		bodyModel[96].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 157
		bodyModel[96].setRotationPoint(38.5F, 3.5F, -6F);

		bodyModel[97].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 160
		bodyModel[97].setRotationPoint(45F, 3F, -6F);

		bodyModel[98].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[98].setRotationPoint(45F, 3F, 3F);

		bodyModel[99].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 162
		bodyModel[99].setRotationPoint(28F, 3F, 3F);

		bodyModel[100].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 163
		bodyModel[100].setRotationPoint(28F, 3F, -6F);

		bodyModel[101].addShapeBox(0F, 0F, 0F, 18, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 164
		bodyModel[101].setRotationPoint(28F, 2F, -6F);

		bodyModel[102].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 168
		bodyModel[102].setRotationPoint(32F, 6F, -6F);

		bodyModel[103].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 168
		bodyModel[103].setRotationPoint(41F, 6F, -6F);

		bodyModel[104].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[104].setRotationPoint(40F, 3F, 2F);

		bodyModel[105].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[105].setRotationPoint(40F, 3F, -5F);

		bodyModel[106].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[106].setRotationPoint(42F, 3F, 2F);

		bodyModel[107].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[107].setRotationPoint(42F, 3F, -5F);

		bodyModel[108].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[108].setRotationPoint(31F, 3F, 2F);

		bodyModel[109].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[109].setRotationPoint(31F, 3F, -5F);

		bodyModel[110].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[110].setRotationPoint(33F, 3F, 2F);

		bodyModel[111].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[111].setRotationPoint(33F, 3F, -5F);

		bodyModel[112].addShapeBox(0F, 0F, 0F, 18, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 138
		bodyModel[112].setRotationPoint(28F, 3.5F, -4F);

		bodyModel[113].addShapeBox(0F, 0F, 0F, 54, 1, 4, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 143
		bodyModel[113].setRotationPoint(-27F, 0.5F, -3F);

		bodyModel[114].addShapeBox(0F, 0F, 0F, 44, 1, 4, 0F,5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 144
		bodyModel[114].setRotationPoint(-22F, 1.5F, -3F);

		bodyModel[115].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 156
		bodyModel[115].setRotationPoint(-44.5F, 3.5F, -6F);

		bodyModel[116].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 157
		bodyModel[116].setRotationPoint(-35.5F, 3.5F, -6F);

		bodyModel[117].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 160
		bodyModel[117].setRotationPoint(-29F, 3F, -6F);

		bodyModel[118].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[118].setRotationPoint(-29F, 3F, 3F);

		bodyModel[119].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 162
		bodyModel[119].setRotationPoint(-46F, 3F, 3F);

		bodyModel[120].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 163
		bodyModel[120].setRotationPoint(-46F, 3F, -6F);

		bodyModel[121].addShapeBox(0F, 0F, 0F, 18, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 164
		bodyModel[121].setRotationPoint(-46F, 2F, -6F);

		bodyModel[122].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 168
		bodyModel[122].setRotationPoint(-42F, 6F, -6F);

		bodyModel[123].addShapeBox(0F, 0F, 0F, 1, 1, 10, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 168
		bodyModel[123].setRotationPoint(-33F, 6F, -6F);

		bodyModel[124].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[124].setRotationPoint(-34F, 3F, 2F);

		bodyModel[125].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[125].setRotationPoint(-34F, 3F, -5F);

		bodyModel[126].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[126].setRotationPoint(-32F, 3F, 2F);

		bodyModel[127].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[127].setRotationPoint(-32F, 3F, -5F);

		bodyModel[128].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[128].setRotationPoint(-43F, 3F, 2F);

		bodyModel[129].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F); // Box 161
		bodyModel[129].setRotationPoint(-43F, 3F, -5F);

		bodyModel[130].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[130].setRotationPoint(-41F, 3F, 2F);

		bodyModel[131].addShapeBox(0F, 0F, 0F, 1, 4, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 161
		bodyModel[131].setRotationPoint(-41F, 3F, -5F);

		bodyModel[132].addShapeBox(0F, 0F, 0F, 18, 3, 6, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F); // Box 138
		bodyModel[132].setRotationPoint(-46F, 3.5F, -4F);

		bodyModel[133].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 163
		bodyModel[133].setRotationPoint(-35.5F, 3.5F, 4F);

		bodyModel[134].addBox(0F, 0F, 0F, 6, 6, 0, 0F); // Box 164
		bodyModel[134].setRotationPoint(-44.5F, 3.5F, 4F);

		bodyModel[135].addBox(0F, 0F, 0F, 3, 2, 2, 0F); // Box 135
		bodyModel[135].setRotationPoint(48.2F, 1F, -2F);

		bodyModel[136].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		bodyModel[136].setRotationPoint(-47F, -2F, 6F);

		bodyModel[137].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 109
		bodyModel[137].setRotationPoint(-48F, -3F, 7F);

		bodyModel[138].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 110
		bodyModel[138].setRotationPoint(-45.5F, -3F, 7F);

		bodyModel[139].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 111
		bodyModel[139].setRotationPoint(-47.5F, -3F, 7F);

		bodyModel[140].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 112
		bodyModel[140].setRotationPoint(-47.5F, -0.5F, 7F);

		bodyModel[141].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -1.75F, -0.5F, 0F, 1.25F, -0.5F, 0F, 1.25F, -0.5F, -0.5F, -1.75F, -0.5F, -0.5F); // Box 113
		bodyModel[141].setRotationPoint(-47.75F, -3F, 7F);

		bodyModel[142].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 83
		bodyModel[142].setRotationPoint(46F, -2F, -10F);

		bodyModel[143].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 109
		bodyModel[143].setRotationPoint(45F, -3F, -10F);

		bodyModel[144].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F); // Box 110
		bodyModel[144].setRotationPoint(47.5F, -3F, -10F);

		bodyModel[145].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 111
		bodyModel[145].setRotationPoint(45.5F, -3F, -10F);

		bodyModel[146].addShapeBox(0F, 0F, 0F, 2, 1, 1, 0F,0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F); // Box 112
		bodyModel[146].setRotationPoint(45.5F, -0.5F, -10F);

		bodyModel[147].addShapeBox(0F, 0F, 0F, 1, 3, 1, 0F,-0.25F, -0.5F, 0F, -0.25F, -0.5F, 0F, -0.25F, -0.5F, -0.5F, -0.25F, -0.5F, -0.5F, -1.75F, -0.5F, 0F, 1.25F, -0.5F, 0F, 1.25F, -0.5F, -0.5F, -1.75F, -0.5F, -0.5F); // Box 113
		bodyModel[147].setRotationPoint(45.25F, -3F, -10F);

		bodyModel[148].addShapeBox(0F, -1F, 0F, 1, 2, 1, 0F,0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 515
		bodyModel[148].setRotationPoint(-50.25F, 4.01F, 2.5F);

		bodyModel[149].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, -0.3F, -0.5F, 0F, -0.3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.4F, -0.5F, -0.5F, -0.4F, -0.5F, -0.5F, -0.1F, 0F, -0.5F, -0.1F); // Box 517
		bodyModel[149].setRotationPoint(-50.25F, 6.01F, 2.6F);

		bodyModel[150].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 515
		bodyModel[150].setRotationPoint(-50.25F, 4.01F, 1.5F);

		bodyModel[151].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, -0.3F, -0.5F, 0F, -0.3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.4F, -0.5F, -0.5F, -0.4F, -0.5F, -0.5F, -0.1F, 0F, -0.5F, -0.1F); // Box 517
		bodyModel[151].setRotationPoint(-50.25F, 5.01F, 1.6F);

		bodyModel[152].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,-0.5F, -0.5F, -0.5F, 1F, -0.5F, -0.5F, 1F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, -0.5F, 1F, 0F, -0.5F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 520
		bodyModel[152].setRotationPoint(-50.25F, 3.01F, 1.5F);

		bodyModel[153].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,-0.5F, -0.5F, -0.5F, 1F, -0.5F, -0.5F, 1F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, -0.5F, 1F, 0F, -0.5F, 1F, 0F, 0F, 0F, 0F, 0F); // Box 520
		bodyModel[153].setRotationPoint(-50.25F, 3.01F, 2.5F);

		bodyModel[154].addShapeBox(0F, -1F, 0F, 1, 2, 1, 0F,0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 515
		bodyModel[154].setRotationPoint(49.75F, 3.01F, -4.5F);

		bodyModel[155].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, -0.3F, -0.5F, 0F, -0.3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.4F, -0.5F, -0.5F, -0.4F, -0.5F, -0.5F, -0.1F, 0F, -0.5F, -0.1F); // Box 517
		bodyModel[155].setRotationPoint(49.75F, 5.01F, -4.4F);

		bodyModel[156].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.5F, 0F, -0.5F, -0.5F, 0F, 0F, 0F, 0F, 0F); // Box 515
		bodyModel[156].setRotationPoint(49.75F, 3.01F, -5.5F);

		bodyModel[157].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,0F, 0F, -0.3F, -0.5F, 0F, -0.3F, -0.5F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, -0.4F, -0.5F, -0.5F, -0.4F, -0.5F, -0.5F, -0.1F, 0F, -0.5F, -0.1F); // Box 517
		bodyModel[157].setRotationPoint(49.75F, 4.01F, -5.4F);

		bodyModel[158].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,1F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 1F, -0.5F, 0F, 1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 520
		bodyModel[158].setRotationPoint(49.25F, 2.01F, -5.5F);

		bodyModel[159].addShapeBox(0F, -1F, 0F, 1, 1, 1, 0F,1F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, -0.5F, 0F, 1F, -0.5F, 0F, 1F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0F, 1F, 0F, 0F); // Box 520
		bodyModel[159].setRotationPoint(49.25F, 2.01F, -4.5F);

		bodyModel[160].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[160].setRotationPoint(-45.5F, 3F, 4.5F);

		bodyModel[161].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[161].setRotationPoint(-41.5F, 3F, 4.5F);

		bodyModel[162].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		bodyModel[162].setRotationPoint(-36.5F, 3F, 4.5F);

		bodyModel[163].addShapeBox(-1F, -1F, 0F, 1, 1, 11, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[163].setRotationPoint(-27F, 9.01F, -5.9F);

		bodyModel[164].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[164].setRotationPoint(-41.5F, 6F, 4.5F);

		bodyModel[165].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[165].setRotationPoint(-43.5F, 6F, 4.5F);

		bodyModel[166].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[166].setRotationPoint(-45.9F, 9.01F, 4.6F);

		bodyModel[167].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[167].setRotationPoint(-32.75F, 6.25F, 5.5F);

		bodyModel[168].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[168].setRotationPoint(-41.75F, 6.25F, 5.5F);

		bodyModel[169].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[169].setRotationPoint(-32.5F, 6F, 4.5F);

		bodyModel[170].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[170].setRotationPoint(-34.5F, 6F, 4.5F);

		bodyModel[171].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0.5F, 0F, 0.3F, 0.5F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0.5F, -0.5F, 0.3F, 0.5F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[171].setRotationPoint(-35.5F, 9.01F, 4.6F);

		bodyModel[172].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[172].setRotationPoint(-41.5F, 6F, -7.5F);

		bodyModel[173].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[173].setRotationPoint(-43.5F, 6F, -7.5F);

		bodyModel[174].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[174].setRotationPoint(-45.9F, 9.01F, -7.4F);

		bodyModel[175].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[175].setRotationPoint(-32.75F, 6.25F, -8F);

		bodyModel[176].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[176].setRotationPoint(-41.75F, 6.25F, -8F);

		bodyModel[177].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[177].setRotationPoint(-32.5F, 6F, -7.5F);

		bodyModel[178].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[178].setRotationPoint(-34.5F, 6F, -7.5F);

		bodyModel[179].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0.5F, 0F, 0.3F, 0.5F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0.5F, -0.5F, 0.3F, 0.5F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[179].setRotationPoint(-35.5F, 9.01F, -7.4F);

		bodyModel[180].addShapeBox(-1F, -1F, 0F, 1, 1, 11, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[180].setRotationPoint(-45.9F, 9.01F, -5.9F);

		bodyModel[181].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F,1.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F); // Box 341
		bodyModel[181].setRotationPoint(-44F, 4F, 4.5F);

		bodyModel[182].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[182].setRotationPoint(-32.5F, 3F, 4.5F);

		bodyModel[183].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[183].setRotationPoint(-45.5F, 3F, -7.5F);

		bodyModel[184].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[184].setRotationPoint(-41.5F, 3F, -7.5F);

		bodyModel[185].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		bodyModel[185].setRotationPoint(-36.5F, 3F, -7.5F);

		bodyModel[186].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[186].setRotationPoint(-32.5F, 3F, -7.5F);

		bodyModel[187].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F,1.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F); // Box 341
		bodyModel[187].setRotationPoint(-44F, 4F, -7.5F);

		bodyModel[188].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 338
		bodyModel[188].setRotationPoint(-37.5F, 3F, -7.5F);

		bodyModel[189].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 338
		bodyModel[189].setRotationPoint(-37.5F, 3F, 4.5F);

		bodyModel[190].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[190].setRotationPoint(28.5F, 3F, 4.5F);

		bodyModel[191].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[191].setRotationPoint(32.5F, 3F, 4.5F);

		bodyModel[192].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		bodyModel[192].setRotationPoint(37.5F, 3F, 4.5F);

		bodyModel[193].addShapeBox(-1F, -1F, 0F, 1, 1, 11, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[193].setRotationPoint(47F, 9.01F, -5.9F);

		bodyModel[194].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[194].setRotationPoint(32.5F, 6F, 4.5F);

		bodyModel[195].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[195].setRotationPoint(30.5F, 6F, 4.5F);

		bodyModel[196].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[196].setRotationPoint(28.1F, 9.01F, 4.6F);

		bodyModel[197].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[197].setRotationPoint(41.25F, 6.25F, 5.5F);

		bodyModel[198].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[198].setRotationPoint(32.25F, 6.25F, 5.5F);

		bodyModel[199].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[199].setRotationPoint(41.5F, 6F, 4.5F);

		bodyModel[200].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[200].setRotationPoint(39.5F, 6F, 4.5F);

		bodyModel[201].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0.5F, 0F, 0.3F, 0.5F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0.5F, -0.5F, 0.3F, 0.5F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[201].setRotationPoint(38.5F, 9.01F, 4.6F);

		bodyModel[202].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[202].setRotationPoint(32.5F, 6F, -7.5F);

		bodyModel[203].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[203].setRotationPoint(30.5F, 6F, -7.5F);

		bodyModel[204].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[204].setRotationPoint(28.1F, 9.01F, -7.4F);

		bodyModel[205].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[205].setRotationPoint(41.25F, 6.25F, -8F);

		bodyModel[206].addShapeBox(0F, 0F, 0F, 1, 1, 1, 0F,0.5F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -0.5F, 0.5F, 0F, -0.5F, 0.5F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, -0.5F, 0.5F, 0.5F, -0.5F); // Box 338
		bodyModel[206].setRotationPoint(32.25F, 6.25F, -8F);

		bodyModel[207].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,0F, 0F, 0F, 1F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[207].setRotationPoint(41.5F, 6F, -7.5F);

		bodyModel[208].addShapeBox(0F, 0F, 0F, 2, 2, 1, 0F,1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 1F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 341
		bodyModel[208].setRotationPoint(39.5F, 6F, -7.5F);

		bodyModel[209].addShapeBox(-1F, -1F, 0F, 9, 1, 2, 0F,0F, 0F, 0.3F, 0.5F, 0F, 0.3F, 0.5F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0.5F, -0.5F, 0.3F, 0.5F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[209].setRotationPoint(38.5F, 9.01F, -7.4F);

		bodyModel[210].addShapeBox(-1F, -1F, 0F, 1, 1, 11, 0F,0F, 0F, 0.3F, 0F, 0F, 0.3F, 0F, 0F, -0.8F, 0F, 0F, -0.8F, 0F, -0.5F, 0.3F, 0F, -0.5F, 0.3F, 0F, -0.5F, -0.8F, 0F, -0.5F, -0.8F); // Box 553
		bodyModel[210].setRotationPoint(28.1F, 9.01F, -5.9F);

		bodyModel[211].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F,1.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F); // Box 341
		bodyModel[211].setRotationPoint(30F, 4F, 4.5F);

		bodyModel[212].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[212].setRotationPoint(41.5F, 3F, 4.5F);

		bodyModel[213].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 1
		bodyModel[213].setRotationPoint(28.5F, 3F, -7.5F);

		bodyModel[214].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[214].setRotationPoint(32.5F, 3F, -7.5F);

		bodyModel[215].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,0F, 0F, 0F, -3F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 9
		bodyModel[215].setRotationPoint(37.5F, 3F, -7.5F);

		bodyModel[216].addShapeBox(0F, 0F, 0F, 4, 1, 1, 0F,-3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, -3F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F, 0F); // Box 6
		bodyModel[216].setRotationPoint(41.5F, 3F, -7.5F);

		bodyModel[217].addShapeBox(0F, 0F, 0F, 15, 2, 1, 0F,1.5F, 0F, 0F, 0.5F, 0F, 0F, 0.5F, 0F, 0F, 1.5F, 0F, 0F, 0.5F, 0F, 0F, -0.5F, 0F, 0F, -0.5F, 0F, 0F, 0.5F, 0F, 0F); // Box 341
		bodyModel[217].setRotationPoint(30F, 4F, -7.5F);

		bodyModel[218].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 338
		bodyModel[218].setRotationPoint(36.5F, 3F, -7.5F);

		bodyModel[219].addBox(0F, 0F, 0F, 1, 1, 1, 0F); // Box 338
		bodyModel[219].setRotationPoint(36.5F, 3F, 4.5F);
	}
	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		for (int i = 0; i < 220; i++) {
			int cargo = ((Freight) entity).getAmmountOfCargo();

			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("a")) {
				if (cargo > 0) {
					bodyModel[i].render(f5);
				}
				if (cargo == 0) {

				}

			} else

				bodyModel[i].render(f5);
		}
	}
}

//this is a dumb attempt to get the logs to render individually as more items were added, i dont get paid enough to fix it

		/*if (cargo == 0) {
			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("a1")&& bodyModel[i].boxName.contains("a2")&& bodyModel[i].boxName.contains("a3")&& bodyModel[i].boxName.contains("a4")&& bodyModel[i].boxName.contains("a5")&& bodyModel[i].boxName.contains("a6")) {

			}else{
			bodyModel[i].render(f5);
			}
		}else if (cargo <= 9) {
			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("a1")) {
				bodyModel[i].render(f5);
			}
		}else if(cargo<=18 && cargo>9) {
			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("a1")&& bodyModel[i].boxName.contains("a2")) {
				bodyModel[i].render(f5);
			}

		}else if(cargo<=27 && cargo>18) {
			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("a1")&& bodyModel[i].boxName.contains("a2")&& bodyModel[i].boxName.contains("a4")) {
				bodyModel[i].render(f5);
			}

		}else if(cargo<=36 && cargo>27) {
			if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("a1")&& bodyModel[i].boxName.contains("a2")&& bodyModel[i].boxName.contains("a4")&& bodyModel[i].boxName.contains("a5")) {
				bodyModel[i].render(f5);
			}

		}else {
			bodyModel[i].render(f5);
		}
		if (bodyModel[i].boxName != null && bodyModel[i].boxName.contains("a1")&& bodyModel[i].boxName.contains("a2")&& bodyModel[i].boxName.contains("a3")&& bodyModel[i].boxName.contains("a4")&& bodyModel[i].boxName.contains("a5")&& bodyModel[i].boxName.contains("a6")) {

		}
		bodyModel[i].render(f5);
	}
	}
}*/