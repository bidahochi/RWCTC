package train.common.entity.rollingStock;

import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants;
import train.common.Traincraft;
import train.common.api.ElectricTrain;
import train.common.core.util.TraincraftUtil;
import train.common.library.GuiIDs;
import train.common.overlaytexture.EnumOverlayFonts;
import train.common.overlaytexture.OTSpecificationDynamic;
import train.common.overlaytexture.OTSpecificationFixed;
import train.common.overlaytexture.OverlayTextureManager;

import java.awt.*;

public class EntityLocoElectricSiemensSD100 extends ElectricTrain {
	public EntityLocoElectricSiemensSD100(World world) {
		super(world);
		initOverlayTextures(OverlayTextureManager.Type.BOTH);
		getOverlayTextureContainer().initSpecificationFixed(new OTSpecificationFixed("sd100_fixed_overlay.png", 38, 52, 11, new Point[]{ new Point(124, 192), new Point(175, 192), new Point(226, 192) }));
		getOverlayTextureContainer().initSpecificationDynamic(new OTSpecificationDynamic("Rollsign", 50, 9, null, EnumOverlayFonts.OxygenSansSmall, 16f, OTSpecificationDynamic.AlignmentMode.ALIGN_CENTER_AND_FILL, new Point[]{ new Point(125, 193), new Point(176, 193), new Point(227, 193) }));
		textureDescriptionMap.put(0, "RTD Original 'A' Car");
		textureDescriptionMap.put(1, "RTD Original 'B' Car");
		textureDescriptionMap.put(2, "MTS 'A' Car");
		textureDescriptionMap.put(3, "MTS 'B' Car");
		textureDescriptionMap.put(4, "SacRT 'A' Car");
		textureDescriptionMap.put(5, "SacRT 'B' Car");
		textureDescriptionMap.put(6, "Silica Elevated 'A' Car (Fictional)");
		textureDescriptionMap.put(7, "Silica Elevated 'B' Car (Fictional)");
		textureDescriptionMap.put(8, "TRAX 'B' Car");
		textureDescriptionMap.put(9, "TRAX 'B' Car");
	}

	public EntityLocoElectricSiemensSD100(World world, double d, double d1, double d2) {
		this(world);
		setPosition(d, d1 + yOffset, d2);
		motionX = 0.0D;
		motionY = 0.0D;
		motionZ = 0.0D;
		prevPosX = d;
		prevPosY = d1;
		prevPosZ = d2;
	}

	@Override
	public void updateRiderPosition() {
		TraincraftUtil.updateRider(this, 4.98, 0.0);
	}

	@Override
	public void setDead() {
		super.setDead();
		isDead = true;
	}

	@Override
	public void pressKey(int i) {
		if (i == 7 && riddenByEntity instanceof EntityPlayer) {
			((EntityPlayer) riddenByEntity).openGui(Traincraft.instance, GuiIDs.LOCO, worldObj, (int) this.posX, (int) this.posY, (int) this.posZ);
		}
	}

	@Override
	protected void writeEntityToNBT(NBTTagCompound nbttagcompound) {
		super.writeEntityToNBT(nbttagcompound);

		nbttagcompound.setShort("fuelTrain", (short) fuelTrain);
		NBTTagList nbttaglist = new NBTTagList();
		for (int i = 0; i < locoInvent.length; i++) {
			if (locoInvent[i] != null) {
				NBTTagCompound nbttagcompound1 = new NBTTagCompound();
				nbttagcompound1.setByte("Slot", (byte) i);
				locoInvent[i].writeToNBT(nbttagcompound1);
				nbttaglist.appendTag(nbttagcompound1);
			}
		}
		nbttagcompound.setTag("Items", nbttaglist);
	}

	@Override
	protected void readEntityFromNBT(NBTTagCompound nbttagcompound) {
		super.readEntityFromNBT(nbttagcompound);

		fuelTrain = nbttagcompound.getShort("fuelTrain");
		NBTTagList nbttaglist = nbttagcompound.getTagList("Items", Constants.NBT.TAG_COMPOUND);
		locoInvent = new ItemStack[getSizeInventory()];
		for (int i = 0; i < nbttaglist.tagCount(); i++) {
			NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound1.getByte("Slot") & 0xff;
			if (j >= 0 && j < locoInvent.length) {
				locoInvent[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
			}
		}
	}

	@Override
	public int getSizeInventory() {
		return inventorySize;
	}

	@Override
	public String getInventoryName() {
		return "Siemens SD100";
	}

	@Override
	public boolean interactFirst(EntityPlayer entityplayer) {
		playerEntity = entityplayer;
		if ((super.interactFirst(entityplayer))) {
			return false;
		}
		if (!worldObj.isRemote) {
			if (riddenByEntity != null && (riddenByEntity instanceof EntityPlayer) && riddenByEntity != entityplayer) {
				return true;
			}
			entityplayer.mountEntity(this);
		}
		return true;
	}

	@Override
	public float getOptimalDistance(EntityMinecart cart) {
		return (1.0F);
	}
	@Override
	public boolean canBeAdjusted(EntityMinecart cart) {
		return canBeAdjusted;
	}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return true;
	}
}
